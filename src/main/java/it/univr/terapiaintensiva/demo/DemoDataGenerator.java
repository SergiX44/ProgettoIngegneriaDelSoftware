package it.univr.terapiaintensiva.demo;

import it.univr.terapiaintensiva.demo.cheats.VitalsCheatsGUI;
import it.univr.terapiaintensiva.monitoring.Alarm;
import it.univr.terapiaintensiva.monitoring.Vitals;
import it.univr.terapiaintensiva.monitoring.VitalsMonitor.VitalsMonitorWorker;
import it.univr.terapiaintensiva.persistence.users.*;
import it.univr.terapiaintensiva.utils.Hashing;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DemoDataGenerator extends VitalsMonitorWorker {
	
	
	public DemoDataGenerator(Vitals patientVitals) {
		super(patientVitals);
	}
	
	
	@Override
	public int systolicBloodPressure() {
		return VitalsCheatsGUI.getInstance().getSBP(patientVitals);
	}
	
	@Override
	public int diastolicBloodPressure() {
		return VitalsCheatsGUI.getInstance().getDBP(patientVitals);
	}
	
	@Override
	public int heartRate() {
		return VitalsCheatsGUI.getInstance().getHeartRate(patientVitals);
	}
	
	@Override
	public double temperature() {
		return VitalsCheatsGUI.getInstance().getTemperature(patientVitals);
	}
	
	@Override
	public List<Alarm> alarms() {
		return VitalsCheatsGUI.getInstance().getAlarms(patientVitals);
	}
	
	
	// *** PERSISTENCE MOCK ***
	
	public static PrimaryDoctor mockPrimaryDoctor() {
		return new PrimaryDoctor(
				"Dott.",
				"Primary",
				Sex.MALE,
				LocalDate.of(1966, 5, 24),
				"Magic",
				"PRIMARY-0000",
				Hashing.sha512("primary"),
				"IlSapere",
				"Tuttologia"
		);
	}
	
	public static Doctor mockDoctor() {
		return new Doctor(
				"Dott.",
				"Doctor",
				Sex.MALE,
				LocalDate.of(1976, 6, 12),
				"Magic",
				"DOCTOR-0000",
				Hashing.sha512("doctor"),
				"Tuttologia"
		);
	}
	
	public static Nurse mockNurse() {
		return new Nurse(
				"Mrs.",
				"Nurse",
				Sex.FEMALE,
				LocalDate.of(1986, 1, 17),
				"Magic",
				"NURSE-0000",
				Hashing.sha512("nurse")
		);
	}
	
	public static List<Patient> mockPatients() {
		List<Patient> mockPatients = new ArrayList<>();
		mockPatients.add(new Patient("Mario", "Rossi", Sex.MALE, LocalDate.now().minusYears(40), "Milano", "CODE-0001"));
		mockPatients.add(new Patient("Paola", "Verdi", Sex.FEMALE, LocalDate.now().minusYears(30), "Roma", "CODE-0002"));
		mockPatients.add(new Patient("Marco", "Violi", Sex.MALE, LocalDate.now().minusYears(20), "Genova", "CODE-0003"));
		return mockPatients;
	}
	
}
