package it.univr.terapiaintensiva.demo.cheats;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import it.univr.terapiaintensiva.gui.GUIFrame;
import it.univr.terapiaintensiva.monitoring.Alarm;
import it.univr.terapiaintensiva.monitoring.Vitals;
import it.univr.terapiaintensiva.persistence.DataManager;
import it.univr.terapiaintensiva.persistence.users.Patient;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.*;
import java.util.List;

import static it.univr.terapiaintensiva.monitoring.VitalsMonitor.VitalsMonitorWorker.*;

public class VitalsCheatsGUI extends GUIFrame<VitalsCheatsActions> implements Observer {
	
	private JPanel contentPane;
	private JSpinner spinnerSbp;
	private JSpinner spinnerSbpVariance;
	private JSpinner spinnerDbp;
	private JSpinner spinnerDbpVariance;
	private JSpinner spinnerHeartRate;
	private JSpinner spinnerHeartRateVariance;
	private JSpinner spinnerTemperature;
	private JSpinner spinnerTemperatureVariance;
	private JButton hyperthermiaButton;
	private JButton hypothermiaButton;
	private JButton hypertensionButton;
	private JButton hypotensionButton;
	private JButton arrhythmiaButton;
	private JButton tachycardiaButton;
	private JButton flutterButton;
	private JButton ventricularFibrillationButton;
	
	private static final Random generator = new Random();
	
	private Vitals currentVitals = null;
	private Map<Vitals, VitalsSetup> vitalsSetups = new HashMap<>();
	
	
	private static VitalsCheatsGUI instance;
	
	private VitalsCheatsGUI() {
		super("Vitals Cheats", DO_NOTHING_ON_CLOSE);
		$$$setupUI$$$();
		setContentPane(contentPane);
		setIconImage(new ImageIcon(this.getClass().getResource("/app-icon.png")).getImage());
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				setState(Frame.ICONIFIED);
			}
		});
		
		actions = new VitalsCheatsActions(this);
		
		spinnerSbp.addChangeListener(actions::onSbpSpinnerChange);
		spinnerDbp.addChangeListener(actions::onDbpSpinnerChange);
		spinnerHeartRate.addChangeListener(actions::onHeartRateSpinnerChange);
		spinnerTemperature.addChangeListener(actions::onTemperatureSpinnerChange);
		spinnerSbpVariance.addChangeListener(actions::onSbpVarianceSpinnerChange);
		spinnerDbpVariance.addChangeListener(actions::onDbpVarianceSpinnerChange);
		spinnerHeartRateVariance.addChangeListener(actions::onHeartRateVarianceSpinnerChange);
		spinnerTemperatureVariance.addChangeListener(actions::onTemperatureVarianceSpinnerChange);
		
		hyperthermiaButton.addActionListener(actions::onHyperthermiaButton);
		hypothermiaButton.addActionListener(actions::onHypothermiaButton);
		hypertensionButton.addActionListener(actions::onHypertensionButton);
		hypotensionButton.addActionListener(actions::onHypotensionButton);
		arrhythmiaButton.addActionListener(actions::onArrhythmiaButton);
		tachycardiaButton.addActionListener(actions::onTachycardiaButton);
		flutterButton.addActionListener(actions::onFlutterButton);
		ventricularFibrillationButton.addActionListener(actions::onVentricularFibrillationButton);
	}
	
	private void createUIComponents() {
		spinnerSbp = new JSpinner(new SpinnerNumberModel(SBP_NORMAL, 0, null, 1));
		spinnerDbp = new JSpinner(new SpinnerNumberModel(DBP_NORMAL, 0, null, 1));
		spinnerHeartRate = new JSpinner(new SpinnerNumberModel(HEARTH_RATE_NORMAL, 0, null, 1));
		spinnerTemperature = new JSpinner(new SpinnerNumberModel(TEMPERATURE_NORMAL, 0, 50, .1));
		
		spinnerSbpVariance = new JSpinner(new SpinnerNumberModel(0, 0, (SBP_NORMAL_MAX - SBP_NORMAL_MIN) / 2, 1));
		spinnerDbpVariance = new JSpinner(new SpinnerNumberModel(0, 0, (DBP_NORMAL_MAX - DBP_NORMAL_MIN) / 2, 1));
		spinnerHeartRateVariance = new JSpinner(new SpinnerNumberModel(0, 0, (HEARTH_RATE_NORMAL_MAX - HEARTH_RATE_NORMAL_MIN) / 2, 1));
		spinnerTemperatureVariance = new JSpinner(new SpinnerNumberModel(0, 0, (TEMPERATURE_NORMAL_MAX - TEMPERATURE_NORMAL_MIN) / 2, .1));
	}
	
	public static VitalsCheatsGUI getInstance() {
		if (instance == null) {
			instance = new VitalsCheatsGUI();
		}
		return instance;
	}
	
	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof DataManager.SharedVariables) {
			if (DataManager.getVariable("closing") != null) {
				o.deleteObservers();
				this.dispose();
			}
			
			Patient focusedPatient = (Patient) DataManager.getVariable("focusedPatient");
			if (focusedPatient != null) {
				setTitle("Vitals Cheats - " + focusedPatient);
				currentVitals = focusedPatient.getCurrentHospitalization().getVitals();
				loadSetupOf(currentVitals);
			}
		}
	}
	
	
	// *** DATA GENERATION INTERFACE ***
	
	public int getSBP(Vitals vitals) {
		int newSbp = getSetupOf(vitals).newSBP();
		if (vitals == currentVitals) {
			spinnerSbp.setValue(newSbp);
		}
		return newSbp;
	}
	
	public int getDBP(Vitals vitals) {
		int newDBP = getSetupOf(vitals).newDBP();
		if (vitals == currentVitals) {
			spinnerDbp.setValue(newDBP);
		}
		return newDBP;
	}
	
	public int getHeartRate(Vitals vitals) {
		int newHeartRate = getSetupOf(vitals).newHeartRate();
		if (vitals == currentVitals) {
			spinnerHeartRate.setValue(newHeartRate);
		}
		return newHeartRate;
	}
	
	public double getTemperature(Vitals vitals) {
		double newTemperature = getSetupOf(vitals).newTemperature();
		if (vitals == currentVitals) {
			spinnerTemperature.setValue(newTemperature);
		}
		return newTemperature;
	}
	
	public List<Alarm> getAlarms(Vitals vitals) {
		List<Alarm> alarms = getSetupOf(vitals).getAlarms();
		if (vitals == currentVitals) {
			resetButtons();
			loadButtonsSetupOf(alarms);
		}
		return alarms;
	}
	
	
	// *** GUI GETTERS ***
	
	Vitals getCurrentVitals() {
		return currentVitals;
	}
	
	int getSbpSpinnerValue() {
		return (int) spinnerSbp.getValue();
	}
	
	int getDbpSpinnerValue() {
		return (int) spinnerDbp.getValue();
	}
	
	int getHeartRateSpinnerValue() {
		return (int) spinnerHeartRate.getValue();
	}
	
	double getTemperatureSpinnerValue() {
		return (double) spinnerTemperature.getValue();
	}
	
	int getSbpVarianceSpinnerValue() {
		return (int) spinnerSbpVariance.getValue();
	}
	
	int getDbpVarianceSpinnerValue() {
		return (int) spinnerDbpVariance.getValue();
	}
	
	int getHeartRateVarianceSpinnerValue() {
		return (int) spinnerHeartRateVariance.getValue();
	}
	
	double getTemperatureVarianceSpinnerValue() {
		return (double) spinnerTemperatureVariance.getValue();
	}
	
	public JButton getHyperthermiaButton() {
		return hyperthermiaButton;
	}
	
	public JButton getHypothermiaButton() {
		return hypothermiaButton;
	}
	
	public JButton getHypertensionButton() {
		return hypertensionButton;
	}
	
	public JButton getHypotensionButton() {
		return hypotensionButton;
	}
	
	public JButton getArrhythmiaButton() {
		return arrhythmiaButton;
	}
	
	public JButton getTachycardiaButton() {
		return tachycardiaButton;
	}
	
	public JButton getFlutterButton() {
		return flutterButton;
	}
	
	public JButton getVentricularFibrillationButton() {
		return ventricularFibrillationButton;
	}
	
	/**
	 * Method generated by IntelliJ IDEA GUI Designer
	 * >>> IMPORTANT!! <<<
	 * DO NOT edit this method OR call it in your code!
	 *
	 * @noinspection ALL
	 */
	private void $$$setupUI$$$() {
		createUIComponents();
		contentPane = new JPanel();
		contentPane.setLayout(new GridLayoutManager(2, 1, new Insets(10, 10, 10, 10), -1, -1));
		contentPane.setMinimumSize(new Dimension(320, 380));
		contentPane.setPreferredSize(new Dimension(320, 380));
		final JPanel panel1 = new JPanel();
		panel1.setLayout(new GridLayoutManager(4, 4, new Insets(0, 0, 0, 0), -1, -1));
		contentPane.add(panel1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
		panel1.setBorder(BorderFactory.createTitledBorder("Readings"));
		panel1.add(spinnerSbp, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label1 = new JLabel();
		label1.setText("SBP");
		panel1.add(label1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label2 = new JLabel();
		label2.setText("BDP");
		panel1.add(label2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label3 = new JLabel();
		label3.setText("Temp");
		panel1.add(label3, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		panel1.add(spinnerDbp, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		panel1.add(spinnerTemperature, new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label4 = new JLabel();
		label4.setEnabled(true);
		Font label4Font = this.$$$getFont$$$(null, -1, 20, label4.getFont());
		if (label4Font != null) label4.setFont(label4Font);
		label4.setText("±");
		panel1.add(label4, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		panel1.add(spinnerSbpVariance, new GridConstraints(0, 3, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label5 = new JLabel();
		label5.setEnabled(true);
		Font label5Font = this.$$$getFont$$$(null, -1, 20, label5.getFont());
		if (label5Font != null) label5.setFont(label5Font);
		label5.setText("±");
		panel1.add(label5, new GridConstraints(1, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label6 = new JLabel();
		label6.setEnabled(true);
		Font label6Font = this.$$$getFont$$$(null, -1, 20, label6.getFont());
		if (label6Font != null) label6.setFont(label6Font);
		label6.setText("±");
		panel1.add(label6, new GridConstraints(3, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		panel1.add(spinnerDbpVariance, new GridConstraints(1, 3, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		panel1.add(spinnerTemperatureVariance, new GridConstraints(3, 3, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		panel1.add(spinnerHeartRate, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label7 = new JLabel();
		label7.setText("BPM");
		panel1.add(label7, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label8 = new JLabel();
		label8.setEnabled(true);
		Font label8Font = this.$$$getFont$$$(null, -1, 20, label8.getFont());
		if (label8Font != null) label8.setFont(label8Font);
		label8.setText("±");
		panel1.add(label8, new GridConstraints(2, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		panel1.add(spinnerHeartRateVariance, new GridConstraints(2, 3, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JPanel panel2 = new JPanel();
		panel2.setLayout(new GridLayoutManager(2, 2, new Insets(0, 0, 0, 0), -1, -1));
		contentPane.add(panel2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
		final JPanel panel3 = new JPanel();
		panel3.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
		panel2.add(panel3, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
		panel3.setBorder(BorderFactory.createTitledBorder("Temperature alarms"));
		hyperthermiaButton = new JButton();
		hyperthermiaButton.setText("Hyperthermia");
		panel3.add(hyperthermiaButton, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		hypothermiaButton = new JButton();
		hypothermiaButton.setText("Hypothermia");
		panel3.add(hypothermiaButton, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JPanel panel4 = new JPanel();
		panel4.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
		panel2.add(panel4, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
		panel4.setBorder(BorderFactory.createTitledBorder("Pressure alarms"));
		hypertensionButton = new JButton();
		hypertensionButton.setText("Hypertension");
		panel4.add(hypertensionButton, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		hypotensionButton = new JButton();
		hypotensionButton.setText("Hypotension");
		panel4.add(hypotensionButton, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JPanel panel5 = new JPanel();
		panel5.setLayout(new GridLayoutManager(4, 1, new Insets(0, 0, 0, 0), -1, -1));
		panel2.add(panel5, new GridConstraints(0, 0, 2, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
		panel5.setBorder(BorderFactory.createTitledBorder("Heart rate alarms"));
		arrhythmiaButton = new JButton();
		arrhythmiaButton.setText("Arrhythmia");
		panel5.add(arrhythmiaButton, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		tachycardiaButton = new JButton();
		tachycardiaButton.setText("Tachycardia");
		panel5.add(tachycardiaButton, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		flutterButton = new JButton();
		flutterButton.setText("Flutter");
		panel5.add(flutterButton, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		ventricularFibrillationButton = new JButton();
		ventricularFibrillationButton.setText("Ventricular fibrillation");
		panel5.add(ventricularFibrillationButton, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
	}
	
	/**
	 * @noinspection ALL
	 */
	private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
		if (currentFont == null) return null;
		String resultName;
		if (fontName == null) {
			resultName = currentFont.getName();
		} else {
			Font testFont = new Font(fontName, Font.PLAIN, 10);
			if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
				resultName = fontName;
			} else {
				resultName = currentFont.getName();
			}
		}
		return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
	}
	
	/**
	 * @noinspection ALL
	 */
	public JComponent $$$getRootComponent$$$() {
		return contentPane;
	}
	
	
	// *** INTERNAL SETUP SYSTEM ***
	
	class VitalsSetup {
		
		private int sbp;
		private int dbp;
		private int heartRate;
		private double temperature;
		private List<Alarm> alarms;
		private int sbpVariance = 0;
		private int dbpVariance = 0;
		private int heartRateVariance = 0;
		private double temperatureVariance = 0;
		
		private VitalsSetup(Vitals vitals) {
			sbp = vitals.getSBP().isEmpty() ? (SBP_NORMAL + randomSBP()) / 2 : vitals.getSBP().getLast().getValue();
			dbp = vitals.getDBP().isEmpty() ? (DBP_NORMAL + randomDBP()) / 2 : vitals.getDBP().getLast().getValue();
			heartRate = vitals.getHeartRate().isEmpty() ? (HEARTH_RATE_NORMAL + randomHeartRate()) / 2 : vitals.getHeartRate().getLast().getValue();
			temperature = vitals.getTemperature().isEmpty() ? (TEMPERATURE_NORMAL + randomTemperature()) / 2 : vitals.getTemperature().getLast().getValue();
			alarms = vitals.getAlarms();
		}
		
		// *** RANDOM INITIALIZATIONS ***
		
		private int randomSBP() {
			return SBP_NORMAL_MIN + generator.nextInt(SBP_NORMAL_MAX - SBP_NORMAL_MIN + 1);
		}
		
		private int randomDBP() {
			return DBP_NORMAL_MIN + generator.nextInt(DBP_NORMAL_MAX - DBP_NORMAL_MIN + 1);
		}
		
		private int randomHeartRate() {
			return HEARTH_RATE_NORMAL_MIN + generator.nextInt(HEARTH_RATE_NORMAL_MAX - HEARTH_RATE_NORMAL_MIN + 1);
		}
		
		private double randomTemperature() {
			return TEMPERATURE_NORMAL_MIN + generator.nextDouble() * (TEMPERATURE_NORMAL_MAX - TEMPERATURE_NORMAL_MIN + 0.1f);
		}
		
		
		// *** VITALS-SPECIFIC DATA GENERATION
		
		int newSBP() {
			return sbp += generator.nextInt(sbpVariance * 2 + 1) - sbpVariance;
		}
		
		int newDBP() {
			return dbp += generator.nextInt(dbpVariance * 2 + 1) - dbpVariance;
		}
		
		int newHeartRate() {
			return heartRate += generator.nextInt(heartRateVariance * 2 + 1) - heartRateVariance;
		}
		
		double newTemperature() {
			return temperature += generator.nextDouble() * (temperatureVariance * 2) - temperatureVariance;
		}
		
		List<Alarm> getAlarms() {
			if (HYPERTENSION_THRESHOLD.test(sbp, dbp)) {
				hypertensionButton.doClick();
			}
			if (HYPOTENSION_THRESHOLD.test(sbp, dbp)) {
				hypotensionButton.doClick();
			}
			if (TACHYCARDIA_THRESHOLD.test(heartRate)) {
				if (FLUTTER_THRESHOLD.test(heartRate)) {
					if (VENTRICULAR_FIBRILLATION_THRESHOLD.test(heartRate)) {
						ventricularFibrillationButton.doClick();
					} else {
						flutterButton.doClick();
					}
				} else {
					tachycardiaButton.doClick();
				}
			}
			if (HYPERTHERMIA_THRESHOLD.test(temperature)) {
				hyperthermiaButton.doClick();
			}
			if (HYPOTHERMIA_THRESHOLD.test(temperature)) {
				hypothermiaButton.doClick();
			}
			return alarms;
		}
		
		
		// *** VITALS-SPECIFIC DATA UPDATE ***
		
		void setSbp(int sbp) {
			this.sbp = sbp;
		}
		
		void setDbp(int dbp) {
			this.dbp = dbp;
		}
		
		void setHeartRate(int heartRate) {
			this.heartRate = heartRate;
		}
		
		void setTemperature(double temperature) {
			this.temperature = temperature;
		}
		
		void setSbpVariance(int sbpVariance) {
			this.sbpVariance = sbpVariance;
		}
		
		void setDbpVariance(int dbpVariance) {
			this.dbpVariance = dbpVariance;
		}
		
		void setHeartRateVariance(int heartRateVariance) {
			this.heartRateVariance = heartRateVariance;
		}
		
		void setTemperatureVariance(double temperatureVariance) {
			this.temperatureVariance = temperatureVariance;
		}
	}
	
	synchronized VitalsSetup getSetupOf(Vitals vitals) {
		if (!vitalsSetups.containsKey(vitals)) {
			vitalsSetups.put(vitals, new VitalsSetup(vitals));
		}
		return vitalsSetups.get(vitals);
	}
	
	private void loadSetupOf(Vitals vitals) {
		VitalsSetup setup = getSetupOf(vitals);
		spinnerSbp.setValue(setup.sbp);
		spinnerDbp.setValue(setup.dbp);
		spinnerHeartRate.setValue(setup.heartRate);
		spinnerTemperature.setValue(setup.temperature);
		spinnerSbpVariance.setValue(setup.sbpVariance);
		spinnerDbpVariance.setValue(setup.dbpVariance);
		spinnerHeartRateVariance.setValue(setup.heartRateVariance);
		spinnerTemperatureVariance.setValue(setup.temperatureVariance);
		resetButtons();
		loadButtonsSetupOf(vitals.getAlarms());
	}
	
	private void resetButtons() {
		arrhythmiaButton.setEnabled(true);
		tachycardiaButton.setEnabled(true);
		flutterButton.setEnabled(true);
		ventricularFibrillationButton.setEnabled(true);
		hypertensionButton.setEnabled(true);
		hypotensionButton.setEnabled(true);
		hyperthermiaButton.setEnabled(true);
		hypothermiaButton.setEnabled(true);
	}
	
	private void loadButtonsSetupOf(List<Alarm> alarms) {
		alarms.stream().filter(Alarm::isUnresolved).forEach(a -> {
			switch (a.getType()) {
				case ARRHYTHMIA:
					arrhythmiaButton.setEnabled(false);
					break;
				case TACHYCARDIA:
					tachycardiaButton.setEnabled(false);
					break;
				case FLUTTER:
					flutterButton.setEnabled(false);
					break;
				case VENTRICULAR_FIBRILLATION:
					ventricularFibrillationButton.setEnabled(false);
					break;
				case HYPERTENSION:
					hypertensionButton.setEnabled(false);
					break;
				case HYPOTENSION:
					hypotensionButton.setEnabled(false);
					break;
				case HYPERTHERMIA:
					hyperthermiaButton.setEnabled(false);
					break;
				case HYPOTHERMIA:
					hypothermiaButton.setEnabled(false);
					break;
			}
		});
	}
	
}
