package it.univr.terapiaintensiva.demo.cheats;

import it.univr.terapiaintensiva.gui.GUIActions;
import it.univr.terapiaintensiva.monitoring.Alarm;
import it.univr.terapiaintensiva.monitoring.AlarmType;

import javax.swing.event.ChangeEvent;
import java.awt.event.ActionEvent;

public class VitalsCheatsActions extends GUIActions<VitalsCheatsGUI> {
	
	VitalsCheatsActions(VitalsCheatsGUI gui) {
		super(gui);
	}
	
	
	public void onSbpSpinnerChange(ChangeEvent e) {
		gui.getSetupOf(gui.getCurrentVitals()).setSbp(gui.getSbpSpinnerValue());
	}
	
	public void onDbpSpinnerChange(ChangeEvent e) {
		gui.getSetupOf(gui.getCurrentVitals()).setDbp(gui.getDbpSpinnerValue());
	}
	
	public void onHeartRateSpinnerChange(ChangeEvent e) {
		gui.getSetupOf(gui.getCurrentVitals()).setHeartRate(gui.getHeartRateSpinnerValue());
	}
	
	public void onTemperatureSpinnerChange(ChangeEvent e) {
		gui.getSetupOf(gui.getCurrentVitals()).setTemperature(gui.getTemperatureSpinnerValue());
	}
	
	public void onSbpVarianceSpinnerChange(ChangeEvent e) {
		gui.getSetupOf(gui.getCurrentVitals()).setSbpVariance(gui.getSbpVarianceSpinnerValue());
	}
	
	public void onDbpVarianceSpinnerChange(ChangeEvent e) {
		gui.getSetupOf(gui.getCurrentVitals()).setDbpVariance(gui.getDbpVarianceSpinnerValue());
	}
	
	public void onHeartRateVarianceSpinnerChange(ChangeEvent e) {
		gui.getSetupOf(gui.getCurrentVitals()).setHeartRateVariance(gui.getHeartRateVarianceSpinnerValue());
	}
	
	public void onTemperatureVarianceSpinnerChange(ChangeEvent e) {
		gui.getSetupOf(gui.getCurrentVitals()).setTemperatureVariance(gui.getTemperatureVarianceSpinnerValue());
	}
	
	
	private void onAlarmButton(Alarm alarm) {
		switch (alarm.getType()) {
			case ARRHYTHMIA:
				this.gui.getArrhythmiaButton().setEnabled(false);
				break;
			case TACHYCARDIA:
				this.gui.getTachycardiaButton().setEnabled(false);
				break;
			case FLUTTER:
				this.gui.getFlutterButton().setEnabled(false);
				break;
			case VENTRICULAR_FIBRILLATION:
				this.gui.getVentricularFibrillationButton().setEnabled(false);
				break;
			case HYPERTENSION:
				this.gui.getHypertensionButton().setEnabled(false);
				break;
			case HYPOTENSION:
				this.gui.getHypotensionButton().setEnabled(false);
				break;
			case HYPERTHERMIA:
				this.gui.getHyperthermiaButton().setEnabled(false);
				break;
			case HYPOTHERMIA:
				this.gui.getHypothermiaButton().setEnabled(false);
				break;
		}
		gui.getSetupOf(gui.getCurrentVitals()).getAlarms().add(alarm);
	}
	
	public void onHyperthermiaButton(ActionEvent e) {
		onAlarmButton(new Alarm(AlarmType.HYPERTHERMIA));
	}
	
	public void onHypothermiaButton(ActionEvent e) {
		onAlarmButton(new Alarm(AlarmType.HYPOTHERMIA));
	}
	
	public void onHypertensionButton(ActionEvent e) {
		onAlarmButton(new Alarm(AlarmType.HYPERTENSION));
	}
	
	public void onHypotensionButton(ActionEvent e) {
		onAlarmButton(new Alarm(AlarmType.HYPOTENSION));
	}
	
	public void onArrhythmiaButton(ActionEvent e) {
		onAlarmButton(new Alarm(AlarmType.ARRHYTHMIA));
	}
	
	public void onTachycardiaButton(ActionEvent e) {
		onAlarmButton(new Alarm(AlarmType.TACHYCARDIA));
	}
	
	public void onFlutterButton(ActionEvent e) {
		onAlarmButton(new Alarm(AlarmType.FLUTTER));
	}
	
	public void onVentricularFibrillationButton(ActionEvent e) {
		onAlarmButton(new Alarm(AlarmType.VENTRICULAR_FIBRILLATION));
	}
	
}
