package it.univr.terapiaintensiva.gui;

/**
 * A generic representation of set of actions that a gui can perform
 */
public abstract class GUIActions<Gui extends GUI<? extends GUIActions<Gui>>> {
	
	protected Gui gui;
	
	public GUIActions(Gui gui) {
		this.gui = gui;
    }
    
}
