package it.univr.terapiaintensiva.gui.hospitalizationHistory;

import it.univr.terapiaintensiva.gui.GUIActions;

import javax.swing.event.ListSelectionEvent;

public class HospitalizationHistoryActions extends GUIActions<HospitalizationHistoryDialog> {

	public HospitalizationHistoryActions(HospitalizationHistoryDialog gui) {
		super(gui);
	}

	public void onPatientSelected(ListSelectionEvent e) {
		if (e.getValueIsAdjusting()) {
			return;
		}
		this.gui.rebuildHospitalizationList();
	}

	public void onHospitalizationSelected(ListSelectionEvent e) {
		if (e.getValueIsAdjusting()) {
			return;
		}
		this.gui.refresh();
	}
}
