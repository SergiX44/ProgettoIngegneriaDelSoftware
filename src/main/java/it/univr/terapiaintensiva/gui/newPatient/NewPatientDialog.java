package it.univr.terapiaintensiva.gui.newPatient;

import com.alee.extended.date.WebDateField;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import it.univr.terapiaintensiva.gui.GUIDialog;
import it.univr.terapiaintensiva.persistence.users.Sex;

import javax.swing.*;
import java.awt.*;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class NewPatientDialog extends GUIDialog<NewPatientActions> {
	
	private JPanel contentPane;
	private JLabel labelHealthCode;
	private JTextField fieldHealthCode;
	private JLabel labelFirstName;
	private JTextField fieldFirstName;
	private JLabel labelLastName;
	private JTextField fieldLastName;
	private JLabel labelSex;
	private JPanel radioButtonsSex;
	private JRadioButton radioFemale;
	private JRadioButton radioMale;
	private JLabel labelBirthPlace;
	private JTextField fieldBirthPlace;
	private JLabel labelBirthday;
	private WebDateField fieldBirthday;
	private JLabel alertMessage;
	private JButton buttonSave;
	private JButton buttonCancel;
	
	
	public NewPatientDialog() {
		super("Dati anagrafici nuovo paziente");
		setContentPane(contentPane);
		getRootPane().setDefaultButton(buttonSave);
		
		actions = new NewPatientActions(this);
		
		buttonSave.addActionListener(actions::onSaveButton);
		buttonCancel.addActionListener(actions::onCancelButton);
	}
	
	
	public void alertMessage(String message) {
		alertMessage.setText(message);
		if (message != null) {
			alertMessage.setVisible(true);
		} else {
			alertMessage.setVisible(false);
		}
		pack();
	}
	
	
	public String getHealthCode() {
		return this.fieldHealthCode.getText();
	}
	
	public String getFirstName() {
		return this.fieldFirstName.getText();
	}
	
	public String getLastName() {
		return this.fieldLastName.getText();
	}
	
	public Sex getSex() {
		if (this.radioMale.isSelected()) {
			return Sex.MALE;
		}
		if (this.radioFemale.isSelected()) {
			return Sex.FEMALE;
		}
		return null;
	}
	
	public String getBirthPlace() {
		return this.fieldBirthPlace.getText();
	}
	
	public LocalDate getBirthday() {
		Date selectedDate = this.fieldBirthday.getDate();
		if (selectedDate == null) {
			return null;
		}
		return selectedDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}
	
	{
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
		$$$setupUI$$$();
	}
	
	/**
	 * Method generated by IntelliJ IDEA GUI Designer
	 * >>> IMPORTANT!! <<<
	 * DO NOT edit this method OR call it in your code!
	 *
	 * @noinspection ALL
	 */
	private void $$$setupUI$$$() {
		contentPane = new JPanel();
		contentPane.setLayout(new GridLayoutManager(2, 1, new Insets(10, 10, 10, 10), -1, -1));
		final JPanel panel1 = new JPanel();
		panel1.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
		contentPane.add(panel1, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, 1, null, null, null, 0, false));
		final Spacer spacer1 = new Spacer();
		panel1.add(spacer1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
		final JPanel panel2 = new JPanel();
		panel2.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1, true, false));
		panel1.add(panel2, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
		buttonSave = new JButton();
		buttonSave.setText("Salva");
		panel2.add(buttonSave, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		buttonCancel = new JButton();
		buttonCancel.setText("Annulla");
		panel2.add(buttonCancel, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JPanel panel3 = new JPanel();
		panel3.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
		contentPane.add(panel3, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
		final JPanel panel4 = new JPanel();
		panel4.setLayout(new GridLayoutManager(6, 3, new Insets(0, 0, 0, 0), -1, -1));
		panel3.add(panel4, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
		labelFirstName = new JLabel();
		labelFirstName.setText("Nome");
		panel4.add(labelFirstName, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		fieldFirstName = new JTextField();
		panel4.add(fieldFirstName, new GridConstraints(1, 1, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
		labelSex = new JLabel();
		labelSex.setText("Sesso");
		panel4.add(labelSex, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(75, 18), null, 0, false));
		labelLastName = new JLabel();
		labelLastName.setText("Cognome");
		panel4.add(labelLastName, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		fieldLastName = new JTextField();
		panel4.add(fieldLastName, new GridConstraints(2, 1, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
		labelBirthPlace = new JLabel();
		labelBirthPlace.setText("Luogo di nascita");
		panel4.add(labelBirthPlace, new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		fieldBirthPlace = new JTextField();
		panel4.add(fieldBirthPlace, new GridConstraints(4, 1, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
		labelBirthday = new JLabel();
		labelBirthday.setText("Data di nascita");
		panel4.add(labelBirthday, new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		fieldBirthday = new WebDateField();
		panel4.add(fieldBirthday, new GridConstraints(5, 1, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		labelHealthCode = new JLabel();
		labelHealthCode.setText("Codice Sanitario");
		panel4.add(labelHealthCode, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		fieldHealthCode = new JTextField();
		panel4.add(fieldHealthCode, new GridConstraints(0, 1, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
		radioButtonsSex = new JPanel();
		radioButtonsSex.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
		panel4.add(radioButtonsSex, new GridConstraints(3, 1, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
		radioFemale = new JRadioButton();
		radioFemale.setText("Femmina");
		radioButtonsSex.add(radioFemale, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		radioMale = new JRadioButton();
		radioMale.setText("Maschio");
		radioButtonsSex.add(radioMale, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		alertMessage = new JLabel();
		alertMessage.setForeground(new Color(-6750208));
		alertMessage.setHorizontalAlignment(10);
		alertMessage.setHorizontalTextPosition(11);
		alertMessage.setText("");
		alertMessage.setVisible(false);
		panel3.add(alertMessage, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		labelFirstName.setLabelFor(fieldFirstName);
		ButtonGroup buttonGroup;
		buttonGroup = new ButtonGroup();
		buttonGroup.add(radioFemale);
		buttonGroup.add(radioMale);
	}
	
	/**
	 * @noinspection ALL
	 */
	public JComponent $$$getRootComponent$$$() {
		return contentPane;
	}
}
