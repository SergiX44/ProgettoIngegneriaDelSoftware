package it.univr.terapiaintensiva.gui.newPatient;

import it.univr.terapiaintensiva.gui.GUIActions;
import it.univr.terapiaintensiva.persistence.DataManager;
import it.univr.terapiaintensiva.persistence.users.Patient;
import it.univr.terapiaintensiva.persistence.users.Sex;

import java.awt.event.ActionEvent;
import java.time.LocalDate;
import java.util.stream.Collectors;

public class NewPatientActions extends GUIActions<NewPatientDialog> {
	
	
	public NewPatientActions(NewPatientDialog newPatientDialog) {
		super(newPatientDialog);
	}
	
	
	public void onSaveButton(ActionEvent e) {
		
		String firstName = this.gui.getFirstName();
		if (firstName == null || firstName.equals("")) {
			this.gui.alertMessage("Inserire il nome del paziente");
			return;
		}
		String lastName = this.gui.getLastName();
		if (lastName == null || lastName.equals("")) {
			this.gui.alertMessage("Inserire il cognome del paziente");
			return;
		}
		Sex sex = this.gui.getSex();
		if (sex == null) {
			this.gui.alertMessage("Selezionare il sesso del paziente");
			return;
		}
		String birthplace = this.gui.getBirthPlace();
		if (birthplace == null || birthplace.equals("")) {
			this.gui.alertMessage("Inserire il luogo di nascita del paziente");
			return;
		}
		LocalDate birthday = this.gui.getBirthday();
		if (birthday == null) {
			this.gui.alertMessage("Inserire la data di nascita del paziente");
			return;
		} else if (birthday.isAfter(LocalDate.now()) || birthday.isBefore(LocalDate.now().minusYears(120))) {
			this.gui.alertMessage("Inserire una data di nascita valida");
			return;
		}
		String healthCode = this.gui.getHealthCode();
		if (healthCode == null || healthCode.equals("")) {
			this.gui.alertMessage("Inserire il codice sanitario del paziente");
			return;
		}
		if (DataManager.getPatients().stream().map(Patient::getHealthCode).collect(Collectors.toList()).contains(healthCode)) {
			this.gui.alertMessage("Il codice sanitario è già in uso");
			return;
		}
		
		DataManager.addPatient(new Patient(firstName, lastName, sex, birthday, birthplace, healthCode));
		
		onCancelButton(null);
	}
	
	public void onCancelButton(ActionEvent e) {
		this.gui.dispose();
	}
}
