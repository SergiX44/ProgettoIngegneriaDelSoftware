package it.univr.terapiaintensiva.gui.manageMedicines;

import it.univr.terapiaintensiva.gui.GUIActions;
import it.univr.terapiaintensiva.persistence.DataManager;
import it.univr.terapiaintensiva.persistence.hospitalization.Medicine;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import java.awt.event.ActionEvent;

public class ManageMedicinesActions extends GUIActions<ManageMedicinesDialog> {
	
	
	public ManageMedicinesActions(ManageMedicinesDialog manageMedicinesDialog) {
		super(manageMedicinesDialog);
	}
	
	
	public void onMedicineSelected(ListSelectionEvent listSelectionEvent) {
		if (listSelectionEvent.getValueIsAdjusting()) {
			return;
		}
		this.gui.refresh();
	}
	
	public void onAddMedicineButton(ActionEvent e) {
		String medicineName = JOptionPane.showInputDialog(this.gui, "Inserire nome del farmaco", "Nuovo farmaco", JOptionPane.PLAIN_MESSAGE);
		
		if (DataManager.getHospital().getMedicines().stream().filter(Medicine::isInUse).anyMatch(m -> m.getCommercialName().equals(medicineName))) {
			JOptionPane.showMessageDialog(this.gui, "Il medicinale è già in lista", "Errore", JOptionPane.WARNING_MESSAGE);
			return;
		}
		
		DataManager.getHospital().getMedicines().add(new Medicine(medicineName));
		this.gui.refresh();
	}
	
	public void onArchiveMedicineButton(ActionEvent e) {
		Medicine medicine = this.gui.getSelectedMedicine();
		if (medicine == null) {
			JOptionPane.showMessageDialog(this.gui, "Selezionare un medicinale da archiviare", "Errore", JOptionPane.ERROR_MESSAGE);
			return;
		}
		int medicineIndex = DataManager.getHospital().getMedicines().indexOf(medicine);
		DataManager.getHospital().getMedicines().get(medicineIndex).setNotUsedAnymore();
		this.gui.refresh();
	}
	
	public void onCloseButton(ActionEvent e) {
		this.gui.dispose();
	}
	
}
