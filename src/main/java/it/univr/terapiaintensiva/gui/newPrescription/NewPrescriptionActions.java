package it.univr.terapiaintensiva.gui.newPrescription;

import it.univr.terapiaintensiva.gui.GUIActions;
import it.univr.terapiaintensiva.persistence.DataManager;
import it.univr.terapiaintensiva.persistence.hospitalization.Prescription;
import it.univr.terapiaintensiva.persistence.users.Doctor;
import it.univr.terapiaintensiva.persistence.users.NursingStaff;
import it.univr.terapiaintensiva.persistence.users.PrimaryDoctor;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import java.awt.event.ActionEvent;
import java.time.LocalDate;
import java.util.ArrayList;

public class NewPrescriptionActions extends GUIActions<NewPrescriptionDialog> {
	
	
	public NewPrescriptionActions(NewPrescriptionDialog newPrescriptionDialog) {
		super(newPrescriptionDialog);
	}
	
	
	public void onSaveButton(ActionEvent e) {
		
		if (this.gui.getSelectedMedicine() == null) {
			this.gui.alertMessage("Selezionare un farmaco da somministrare");
			return;
		}
		
		LocalDate start = this.gui.getBeginningDate();
		LocalDate end = this.gui.getEndingDate();
		if (start == null || end == null || start.isAfter(end) || start.isAfter(LocalDate.now())) {
			this.gui.alertMessage("Data di inizio o di fine terapia non valida");
			return;
		}
		
		if (this.gui.getDailyDoses() == 0) {
			this.gui.alertMessage("Non è possibile somministrare dosi nulle");
			return;
		}
		
		ArrayList<Double> doses = new ArrayList<>();
		DefaultListModel<String> dosesModel = this.gui.getDosesModel();
		
		for (int i = 0; i < dosesModel.size(); i++) {
			try {
				Double el = Double.parseDouble(dosesModel.getElementAt(i));
				if (el <= 0F) {
					this.gui.alertMessage("Non è possibile somministrare dosi nulle");
					return;
				}
				doses.add(el);
			} catch (NumberFormatException ex) {
				this.gui.alertMessage("Valore di una dose non valido");
				return;
			}
		}
		
		if (this.gui.getQuantityUnit().equals("")) {
			this.gui.alertMessage("Inserire un'unità di misura");
			return;
		}
		
		Doctor doc;
		NursingStaff currentUser = DataManager.getCurrentUser();
		if (currentUser instanceof Doctor) {
			doc = (Doctor) currentUser;
		} else {
			doc = (PrimaryDoctor) currentUser;
		}
		
		this.gui.getPatient().getCurrentHospitalization().getPrescriptions().add(
				new Prescription(
						doc,
						this.gui.getSelectedMedicine(),
						start,
						end,
						this.gui.getDailyDoses(),
						doses,
						this.gui.getQuantityUnit()
				)
		);
		onCancelButton(null);
	}
	
	public void onCancelButton(ActionEvent e) {
		this.gui.dispose();
	}
	
	public void onDailyDosesChange(ChangeEvent e) {
		int newDailyDoses = this.gui.getDailyDoses();
		
		
		while (newDailyDoses != this.gui.getDosesModel().size()) {
			if (newDailyDoses < this.gui.getDosesModel().size()) {
				this.gui.getDosesModel().removeElementAt(this.gui.getDosesModel().size() - 1);
			} else {
				this.gui.getDosesModel().addElement("0.0");
			}
		}
	}
}
