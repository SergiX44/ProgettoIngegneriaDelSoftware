package it.univr.terapiaintensiva.gui.newAdministration;

import it.univr.terapiaintensiva.gui.GUIActions;
import it.univr.terapiaintensiva.persistence.hospitalization.Administration;
import it.univr.terapiaintensiva.persistence.hospitalization.Prescription;

import java.awt.event.ActionEvent;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

public class NewAdministrationActions extends GUIActions<NewAdministrationDialog> {
	
	public NewAdministrationActions(NewAdministrationDialog newAdministrationDialog) {
		super(newAdministrationDialog);
	}
	
	
	public void onSaveButton(ActionEvent e) {
		
		Prescription prescription = this.gui.getSelectedPrescription();
		if (prescription == null) {
			this.gui.alertMessage("Selezionare una prescrizione");
			return;
		}
		
		LocalDate localDate = this.gui.getDate();
		
		if (localDate.isAfter(LocalDate.now())) {
			this.gui.alertMessage("Inserire una data valida");
			return;
		}
		
		LocalTime localTime;
		try {
			localTime = LocalTime.parse(this.gui.getTime(), DateTimeFormatter.ofPattern("HH.mm"));
		} catch (DateTimeParseException ex) {
			this.gui.alertMessage("Inserire un orario valido");
			return;
		}
		
		if (localTime.isAfter(LocalTime.now())) {
			this.gui.alertMessage("Inserire un orario valido");
			return;
		}
		
		List<Double> doseQuantity = this.gui.getSelectedPrescription().getDoseQuantity();
		
		this.gui.getPatient()
				.getCurrentHospitalization()
				.getAdministrations()
				.stream()
				.filter(a -> a.getPrescription().equals(prescription))
				.filter(a -> a.getTime().toLocalDate().isEqual(localDate))
				.forEach(administration -> doseQuantity.remove(administration.getDose()));
		
		if (!doseQuantity.contains(this.gui.getDose())) {
			this.gui.alertMessage("Questa dose è già stata somministrata nella data selezionata");
			return;
		}
		
		this.gui.getPatient()
				.getCurrentHospitalization()
				.getAdministrations()
				.add(new Administration(prescription, LocalDateTime.of(localDate, localTime), this.gui.getDose()).setNotes(this.gui.getNotes()));
		
		onCancelButton(null);
	}
	
	public void onCancelButton(ActionEvent e) {
		this.gui.dispose();
	}
	
	public void onPrescriptionChange(ActionEvent e) {
		this.gui.getDoseDropdown().removeAllItems();
		this.gui.getSelectedPrescription().getDoseQuantity().forEach(this.gui.getDoseDropdown()::addItem);
	}
}
