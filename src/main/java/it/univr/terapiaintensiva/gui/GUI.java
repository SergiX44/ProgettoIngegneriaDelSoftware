package it.univr.terapiaintensiva.gui;


interface GUI<Actions extends GUIActions<? extends GUI<Actions>>> {
	
	void launch();
	
	void alignCenter();
	
	void refresh();
}
