package it.univr.terapiaintensiva.gui;

import javax.swing.*;
import java.awt.*;

/**
 * A generic gui
 */
public abstract class GUIDialog<Actions extends GUIActions<? extends GUIDialog<Actions>>> extends JDialog implements GUI<Actions> {
	
	protected Actions actions;
	
	public GUIDialog(String title, Dialog.ModalityType modalityType, int defaultCloseOperation) {
		setTitle(title);
		setModalityType(modalityType);
		setDefaultCloseOperation(defaultCloseOperation);
	}
	
	public GUIDialog(String title) {
		this(title, ModalityType.APPLICATION_MODAL, DISPOSE_ON_CLOSE);
	}
	
	@Override
	public void launch() {
		this.pack();
		this.alignCenter();
		this.setVisible(true);
	}
	
	@Override
	public void alignCenter() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(
				screenSize.width / 2 - this.getSize().width / 2,
				screenSize.height / 2 - this.getSize().height / 2
		);
	}
	
	@Override
	public void refresh() {
		// do nothing here
	}
}