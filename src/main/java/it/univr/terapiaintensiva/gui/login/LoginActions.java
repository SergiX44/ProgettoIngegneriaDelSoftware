package it.univr.terapiaintensiva.gui.login;

import it.univr.terapiaintensiva.gui.GUIActions;
import it.univr.terapiaintensiva.gui.main.MainGUI;
import it.univr.terapiaintensiva.persistence.DataManager;
import it.univr.terapiaintensiva.persistence.users.NursingStaff;
import it.univr.terapiaintensiva.utils.Hashing;

import java.awt.event.ActionEvent;


public class LoginActions extends GUIActions<LoginGUI> {
	
	
	public LoginActions(LoginGUI loginDialog) {
		super(loginDialog);
	}
	
	
	public void onLoginButton(ActionEvent e) {
		// search for user
		NursingStaff user = DataManager.searchNursingStaff(nursingStaff -> nursingStaff.getMedId().equals(this.gui.getMedID())).findFirst().orElse(null);
		
		if (user != null) {
			String passHash = Hashing.sha512(this.gui.getPassword());
			if (passHash == null) {
				// no hashing algorithm in jvm
				this.gui.alertMessage("Errore fatale durante il login, non è possibile autenticarsi");

			} else if (user.getPasswordHash().equals(passHash)) {

				DataManager.setCurrentUser(user);
				
				// enter main page
				this.gui.dispose();
				new MainGUI().launch();
				
			} else {
				this.gui.alertMessage("Password non valida");
			}
		} else { // invalid credentials
			this.gui.alertMessage("MedID non valido");
		}
	}
}
