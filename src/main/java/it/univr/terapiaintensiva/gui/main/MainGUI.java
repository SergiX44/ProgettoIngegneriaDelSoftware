package it.univr.terapiaintensiva.gui.main;

import com.alee.laf.menu.WebMenuBar;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import it.univr.terapiaintensiva.gui.GUIFrame;
import it.univr.terapiaintensiva.gui.JAutoSuppliedList;
import it.univr.terapiaintensiva.monitoring.Alarm;
import it.univr.terapiaintensiva.monitoring.Vitals;
import it.univr.terapiaintensiva.persistence.DataManager;
import it.univr.terapiaintensiva.persistence.hospitalization.Administration;
import it.univr.terapiaintensiva.persistence.hospitalization.Prescription;
import it.univr.terapiaintensiva.persistence.users.*;
import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.XYChart;

import javax.swing.*;
import java.awt.*;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class MainGUI extends GUIFrame<MainActions> {
	
	private JPanel contentPane;
	private JMenuBar menu;
	private JAutoSuppliedList<Patient> hospitalizedPatientsList;
	private JTextArea diagnosis;
	private JTextField healthCode;
	private JTextField firstName;
	private JTextField lastName;
	private JTextField sex;
	private JTextField birthDate;
	private JTextField birthPlace;
	private JAutoSuppliedList<Prescription> prescriptionsList;
	private JAutoSuppliedList<Administration> administrationsList;
	private JAutoSuppliedList<AlarmDisplay> alarmsList;
	private XChartPanel<XYChart> pressuresChartPanel;
	private XChartPanel<XYChart> temperatureChartPanel;
	private XChartPanel<XYChart> heartRateChartPanel;
	
	private ChartUpdater sbpReadingChartUpdater;
	private ChartUpdater dbpReadingChartUpdater;
	private ChartUpdater heartRateReadingChartUpdater;
	private ChartUpdater temperatureReadingChartUpdater;
	
	private ScheduledExecutorService alertUpdateThread = Executors.newSingleThreadScheduledExecutor();
	private List<AlarmDisplay> activeAlarms = new ArrayList<>();
	
	public MainGUI() {
		super("Terapia Intensiva", DO_NOTHING_ON_CLOSE);
		$$$setupUI$$$();
		setContentPane(contentPane);
		setIconImage(new ImageIcon(this.getClass().getResource("/app-icon.png")).getImage());
		
		this.actions = new MainActions(this);
		
		setupMenu();
		setJMenuBar(menu);
		
		DataManager.startMonitor();
		
		this.hospitalizedPatientsList.addListSelectionListener(actions::onPatientSelected);
		if (DataManager.getCurrentUser() instanceof Doctor) {
			this.alarmsList.addMouseListener(actions.onAlarmClick());
		}
		this.addWindowListener(actions.onWindowClosing());
		
		if (hospitalizedPatientsList.getModel().getSize() != 0) {
			hospitalizedPatientsList.setSelectedIndex(0);
		}
		
		sbpReadingChartUpdater.execute();
		dbpReadingChartUpdater.execute();
		temperatureReadingChartUpdater.execute();
		heartRateReadingChartUpdater.execute();
		
		refresh();
		
		alertUpdateThread.scheduleAtFixedRate(alarmsList::rebuild, 0, 1, TimeUnit.SECONDS);
	}
	
	private void createUIComponents() {
		this.hospitalizedPatientsList = new JAutoSuppliedList<>(DataManager::getHospitalizedPatients);
		this.prescriptionsList = new JAutoSuppliedList<>(() -> getSelectedPatient().getCurrentHospitalization().getPrescriptions());
		this.administrationsList = new JAutoSuppliedList<>(
				() -> getSelectedPatient().getCurrentHospitalization().getAdministrations().stream()
						.filter(a -> a.getTime().isAfter(LocalDateTime.now().minusDays(2)))
						.collect(Collectors.toList())
		);
		this.alarmsList = new JAutoSuppliedList<>(this::getActiveAlarms);
		
		
		XYChart pressuresChart = ChartBuilderFactory.getPressureChart();
		this.pressuresChartPanel = new XChartPanel<>(pressuresChart);
		sbpReadingChartUpdater = new ChartUpdater(pressuresChartPanel, "SBP");
		dbpReadingChartUpdater = new ChartUpdater(pressuresChartPanel, "DBP");
		
		XYChart temperatureChart = ChartBuilderFactory.getTemperatureChart();
		this.temperatureChartPanel = new XChartPanel<>(temperatureChart);
		temperatureReadingChartUpdater = new ChartUpdater(temperatureChartPanel, "Temperature");
		
		XYChart heartRateChart = ChartBuilderFactory.getHeartRateChart();
		this.heartRateChartPanel = new XChartPanel<>(heartRateChart);
		heartRateReadingChartUpdater = new ChartUpdater(heartRateChartPanel, "HeartRate");
	}
	
	private void setupMenu() {
		
		this.menu = new WebMenuBar();
		((WebMenuBar) this.menu).setUndecorated(true);
		
		JMenu fileMenu = new JMenu("File");
		JMenuItem loadScenario = new JMenuItem("Carica scenario...");
		loadScenario.addActionListener(actions::onLoadScenario);
		fileMenu.add(loadScenario);
		JMenuItem saveScenario = new JMenuItem("Salva scenario...");
		saveScenario.addActionListener(actions::onSaveScenario);
		fileMenu.add(saveScenario);
		fileMenu.addSeparator();
		JMenuItem logout = new JMenuItem("Termina sessione");
		logout.addActionListener(actions::onLogoutButton);
		fileMenu.add(logout);
		this.menu.add(fileMenu);
		
		JMenu patientMenu = new JMenu("Paziente");
		JMenuItem addPatient = new JMenuItem("Aggiungi paziente");
		addPatient.addActionListener(actions::onAddPatientButton);
		addPatient.setEnabled(false);
		patientMenu.add(addPatient);
		patientMenu.addSeparator();
		JMenuItem newHospitalization = new JMenuItem("Nuovo ricovero");
		newHospitalization.addActionListener(actions::onNewHospitalization);
		newHospitalization.setEnabled(false);
		patientMenu.add(newHospitalization);
		this.menu.add(patientMenu);
		
		JMenu hospitalizationMenu = new JMenu("Ricovero");
		JMenuItem newPrescription = new JMenuItem("Nuova prescrizione");
		newPrescription.addActionListener(actions::onNewPrescription);
		newPrescription.setEnabled(false);
		hospitalizationMenu.add(newPrescription);
		JMenuItem newAdministration = new JMenuItem("Nuova somministrazione");
		newAdministration.addActionListener(actions::onNewAdministration);
		newAdministration.setEnabled(false);
		hospitalizationMenu.add(newAdministration);
		hospitalizationMenu.addSeparator();
		JMenuItem dismissPatient = new JMenuItem("Dimetti paziente");
		dismissPatient.addActionListener(actions::onDismissPatient);
		dismissPatient.setEnabled(false);
		hospitalizationMenu.add(dismissPatient);
		hospitalizationMenu.addSeparator();
		JMenuItem printReport = new JMenuItem("Stampa report ricoveri");
		printReport.addActionListener(actions::onPrintReport);
		printReport.setEnabled(false);
		hospitalizationMenu.add(printReport);
		JMenuItem hospitalizationHistory = new JMenuItem("Cronologia ricoveri");
		hospitalizationHistory.addActionListener(actions::onHospitalizationHistory);
		hospitalizationMenu.add(hospitalizationHistory);
		this.menu.add(hospitalizationMenu);
		
		JMenu medicinesMenu = new JMenu("Medicinali");
		JMenuItem manageMedicines = new JMenuItem("Gestisci medicinali");
		manageMedicines.addActionListener(actions::onManageMedicines);
		medicinesMenu.add(manageMedicines);
		this.menu.add(medicinesMenu);
		
		this.menu.add(new JSeparator(JSeparator.VERTICAL));
		
		NursingStaff user = DataManager.getCurrentUser();
		JMenu userNameMenu = new JMenu(MessageFormat.format("{0} {1}", user.getFirstName(), user.getLastName()));
		userNameMenu.setEnabled(false);
		
		this.menu.add(userNameMenu);
	}
	
	@Override
	public void dispose() {
		sbpReadingChartUpdater.cancel(true);
		dbpReadingChartUpdater.cancel(true);
		heartRateReadingChartUpdater.cancel(true);
		temperatureReadingChartUpdater.cancel(true);
		alertUpdateThread.shutdownNow();
		super.dispose();
	}
	
	@Override
	public void refresh() {
		
		// *** UPDATE HOSPITALIZED PATIENTS LIST ***
		
		int sizeBefore = hospitalizedPatientsList.getModel().getSize();
		int oldIndex = hospitalizedPatientsList.getSelectedIndex();
		
		this.hospitalizedPatientsList.rebuild();
		
		if (sizeBefore != hospitalizedPatientsList.getModel().getSize()) {
			hospitalizedPatientsList.setSelectedIndex(hospitalizedPatientsList.getModel().getSize() - 1);
		} else {
			hospitalizedPatientsList.setSelectedIndex(oldIndex);
		}
		
		updateInterface();
	}
	
	public void updateInterface() {
		// *** UPDATE MENU ***
		
		// if it's a primary doctor
		if (DataManager.getCurrentUser() instanceof PrimaryDoctor) {
			this.menu.getMenu(2).getItem(3).setEnabled(true); // dismissPatient
			this.menu.getMenu(2).getItem(5).setEnabled(true); // printReport
		}
		
		// if it's a doctor
		if (DataManager.getCurrentUser() instanceof Doctor) {
			this.menu.getMenu(1).getItem(2).setEnabled(true); // newHospitalization
			this.menu.getMenu(2).getItem(0).setEnabled(true); // newPrescription
		}
		
		// if is a nurse
		if (DataManager.getCurrentUser() instanceof Nurse) {
			this.menu.getMenu(1).getItem(0).setEnabled(true); // newPatient
			this.menu.getMenu(2).getItem(1).setEnabled(true); // newAdministration
		}
		
		
		// *** UPDATE PATIENT'S DATA ***
		Patient selectedPatient = getSelectedPatient();
		if (selectedPatient == null) {
			if (this.menu != null) {
				this.menu.getMenu(2).getItem(0).setEnabled(false);
				this.menu.getMenu(2).getItem(1).setEnabled(false);
				this.menu.getMenu(2).getItem(3).setEnabled(false);
			}
			
			this.healthCode.setText(null);
			this.firstName.setText(null);
			this.lastName.setText(null);
			this.sex.setText(null);
			this.birthDate.setText(null);
			this.birthPlace.setText(null);
			this.diagnosis.setText(null);
			
			this.sbpReadingChartUpdater.changeReadingSupplier(null);
			this.dbpReadingChartUpdater.changeReadingSupplier(null);
			this.temperatureReadingChartUpdater.changeReadingSupplier(null);
			this.heartRateReadingChartUpdater.changeReadingSupplier(null);
			
			this.prescriptionsList.clear();
			this.administrationsList.clear();
			
			return;
		}
		this.healthCode.setText(selectedPatient.getHealthCode());
		this.firstName.setText(selectedPatient.getFirstName());
		this.lastName.setText(selectedPatient.getLastName());
		this.sex.setText(selectedPatient.getSex().toString());
		this.birthDate.setText(selectedPatient.getBirthDate().format(DateTimeFormatter.ofPattern("dd/MM/yyy")));
		this.birthPlace.setText(selectedPatient.getBirthPlace());
		this.diagnosis.setText(selectedPatient.getCurrentHospitalization().getDiagnosis());
		
		// rebuild patient's lists
		this.prescriptionsList.rebuild();
		this.administrationsList.rebuild();
		
		Vitals patientVitals = selectedPatient.getCurrentHospitalization().getVitals();
		this.sbpReadingChartUpdater.changeReadingSupplier(patientVitals.getSBP());
		this.dbpReadingChartUpdater.changeReadingSupplier(patientVitals.getDBP());
		this.temperatureReadingChartUpdater.changeReadingSupplier(patientVitals.getTemperature());
		this.heartRateReadingChartUpdater.changeReadingSupplier(patientVitals.getHeartRate());
	}
	
	
	public Patient getSelectedPatient() {
		Patient selectedPatient = hospitalizedPatientsList.getSelectedValue();
		DataManager.setVariable("focusedPatient", selectedPatient);
		return selectedPatient;
	}
	
	public List<AlarmDisplay> getActiveAlarms() {
		DataManager.getHospitalizedPatients()
				.forEach(p -> p.getCurrentHospitalization().getVitals().getAlarms().stream()
						.filter(Alarm::isUnresolved)
						.forEach(alarm -> {
							if (activeAlarms.stream().noneMatch(alarmDisplay -> alarmDisplay.getAlarm().equals(alarm))) {
								activeAlarms.add(new AlarmDisplay(p, alarm));
							}
						}));
		activeAlarms.sort((o1, o2) -> o2.getAlarm().getType().getSeverityLevel() - o1.getAlarm().getType().getSeverityLevel());
		return activeAlarms;
	}
	
	/**
	 * Method generated by IntelliJ IDEA GUI Designer
	 * >>> IMPORTANT!! <<<
	 * DO NOT edit this method OR call it in your code!
	 *
	 * @noinspection ALL
	 */
	private void $$$setupUI$$$() {
		createUIComponents();
		contentPane = new JPanel();
		contentPane.setLayout(new GridLayoutManager(1, 1, new Insets(10, 15, 10, 15), -1, -1));
		contentPane.setMinimumSize(new Dimension(700, 200));
		contentPane.setPreferredSize(new Dimension(1280, 720));
		final JSplitPane splitPane1 = new JSplitPane();
		splitPane1.setContinuousLayout(true);
		splitPane1.setDividerLocation(180);
		contentPane.add(splitPane1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(200, 200), null, 0, false));
		final JPanel panel1 = new JPanel();
		panel1.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		panel1.setMinimumSize(new Dimension(150, -1));
		panel1.setPreferredSize(new Dimension(180, -1));
		splitPane1.setLeftComponent(panel1);
		panel1.setBorder(BorderFactory.createTitledBorder("Ricoveri"));
		hospitalizedPatientsList.setSelectionMode(0);
		hospitalizedPatientsList.setValueIsAdjusting(true);
		panel1.add(hospitalizedPatientsList, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(150, 50), null, 0, false));
		final JSplitPane splitPane2 = new JSplitPane();
		splitPane2.setContinuousLayout(true);
		splitPane2.setDividerLocation(700);
		splitPane1.setRightComponent(splitPane2);
		final JSplitPane splitPane3 = new JSplitPane();
		splitPane3.setContinuousLayout(true);
		splitPane3.setDividerLocation(580);
		splitPane3.setOrientation(0);
		splitPane2.setLeftComponent(splitPane3);
		final JPanel panel2 = new JPanel();
		panel2.setLayout(new GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1));
		panel2.setMinimumSize(new Dimension(200, 400));
		panel2.setPreferredSize(new Dimension(800, 400));
		panel2.setVerifyInputWhenFocusTarget(true);
		splitPane3.setLeftComponent(panel2);
		panel2.setBorder(BorderFactory.createTitledBorder("Parametri vitali"));
		pressuresChartPanel.setEnabled(false);
		panel2.add(pressuresChartPanel, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
		panel2.add(temperatureChartPanel, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
		panel2.add(heartRateChartPanel, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
		final JPanel panel3 = new JPanel();
		panel3.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		panel3.setMinimumSize(new Dimension(200, 75));
		panel3.setPreferredSize(new Dimension(200, -1));
		splitPane3.setRightComponent(panel3);
		panel3.setBorder(BorderFactory.createTitledBorder("Terapia"));
		final JTabbedPane tabbedPane1 = new JTabbedPane();
		tabbedPane1.setTabLayoutPolicy(0);
		tabbedPane1.setTabPlacement(1);
		panel3.add(tabbedPane1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(200, 200), null, 0, false));
		final JPanel panel4 = new JPanel();
		panel4.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		tabbedPane1.addTab("Prescrizioni", panel4);
		panel4.add(prescriptionsList, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
		final JPanel panel5 = new JPanel();
		panel5.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		tabbedPane1.addTab("Somministrazioni", panel5);
		administrationsList.setSelectionMode(0);
		administrationsList.setValueIsAdjusting(true);
		panel5.add(administrationsList, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(150, 50), null, 0, false));
		final JSplitPane splitPane4 = new JSplitPane();
		splitPane4.setContinuousLayout(true);
		splitPane4.setDividerLocation(250);
		splitPane4.setOrientation(0);
		splitPane2.setRightComponent(splitPane4);
		final JPanel panel6 = new JPanel();
		panel6.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		panel6.setMinimumSize(new Dimension(200, 150));
		panel6.setPreferredSize(new Dimension(200, 250));
		splitPane4.setRightComponent(panel6);
		panel6.setBorder(BorderFactory.createTitledBorder("Allarmi"));
		panel6.add(alarmsList, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(150, 50), null, 0, false));
		final JPanel panel7 = new JPanel();
		panel7.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		panel7.setMinimumSize(new Dimension(300, 250));
		panel7.setPreferredSize(new Dimension(700, 300));
		splitPane4.setLeftComponent(panel7);
		panel7.setBorder(BorderFactory.createTitledBorder("Anagrafica paziente"));
		final JTabbedPane tabbedPane2 = new JTabbedPane();
		panel7.add(tabbedPane2, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(200, 200), null, 0, false));
		final JPanel panel8 = new JPanel();
		panel8.setLayout(new GridLayoutManager(6, 2, new Insets(5, 5, 5, 5), -1, -1));
		tabbedPane2.addTab("Paziente", panel8);
		final JLabel label1 = new JLabel();
		label1.setText("Nome");
		panel8.add(label1, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label2 = new JLabel();
		label2.setText("Cognome");
		panel8.add(label2, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label3 = new JLabel();
		label3.setText("Sesso");
		panel8.add(label3, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label4 = new JLabel();
		label4.setText("Codice Sanitario");
		panel8.add(label4, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label5 = new JLabel();
		label5.setText("Data di nascita");
		panel8.add(label5, new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label6 = new JLabel();
		label6.setText("Luogo di Nascita");
		panel8.add(label6, new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		healthCode = new JTextField();
		healthCode.setEditable(false);
		healthCode.setFocusable(false);
		healthCode.setHorizontalAlignment(0);
		panel8.add(healthCode, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
		firstName = new JTextField();
		firstName.setEditable(false);
		firstName.setFocusable(false);
		firstName.setHorizontalAlignment(0);
		panel8.add(firstName, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
		lastName = new JTextField();
		lastName.setEditable(false);
		lastName.setFocusable(false);
		lastName.setHorizontalAlignment(0);
		panel8.add(lastName, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
		sex = new JTextField();
		sex.setEditable(false);
		sex.setFocusable(false);
		sex.setHorizontalAlignment(0);
		panel8.add(sex, new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
		birthDate = new JTextField();
		birthDate.setEditable(false);
		birthDate.setFocusable(false);
		birthDate.setHorizontalAlignment(0);
		birthDate.setText("");
		panel8.add(birthDate, new GridConstraints(4, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
		birthPlace = new JTextField();
		birthPlace.setEditable(false);
		birthPlace.setFocusable(false);
		birthPlace.setHorizontalAlignment(0);
		panel8.add(birthPlace, new GridConstraints(5, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
		final JPanel panel9 = new JPanel();
		panel9.setLayout(new GridLayoutManager(1, 1, new Insets(5, 5, 5, 5), -1, -1));
		tabbedPane2.addTab("Diagnosi", panel9);
		diagnosis = new JTextArea();
		diagnosis.setEditable(false);
		panel9.add(diagnosis, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(150, 50), null, 0, false));
	}
	
	/**
	 * @noinspection ALL
	 */
	public JComponent $$$getRootComponent$$$() {
		return contentPane;
	}
}
