package it.univr.terapiaintensiva.gui.main;

import it.univr.terapiaintensiva.gui.GUIActions;
import it.univr.terapiaintensiva.gui.JAutoSuppliedList;
import it.univr.terapiaintensiva.gui.dismissPatient.DismissPatientDialog;
import it.univr.terapiaintensiva.gui.hospitalizationHistory.HospitalizationHistoryDialog;
import it.univr.terapiaintensiva.gui.login.LoginGUI;
import it.univr.terapiaintensiva.gui.manageMedicines.ManageMedicinesDialog;
import it.univr.terapiaintensiva.gui.newAdministration.NewAdministrationDialog;
import it.univr.terapiaintensiva.gui.newHospitalization.NewHospitalizationDialog;
import it.univr.terapiaintensiva.gui.newPatient.NewPatientDialog;
import it.univr.terapiaintensiva.gui.newPrescription.NewPrescriptionDialog;
import it.univr.terapiaintensiva.gui.printReport.PrintReportDialog;
import it.univr.terapiaintensiva.monitoring.Alarm;
import it.univr.terapiaintensiva.persistence.DataManager;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;

public class MainActions extends GUIActions<MainGUI> {
	
	
	public MainActions(MainGUI mainGui) {
		super(mainGui);
	}
	
	
	public void onLogoutButton(ActionEvent e) {
		DataManager.setCurrentUser(null);
		
		// enter main page
		this.gui.dispose();
		DataManager.scheduleStopMonitor();
		new LoginGUI().launch();
	}
	
	public void onLoadScenario(ActionEvent e) {
		JFileChooser fc = new JFileChooser(System.getProperty("user.dir"));
		fc.setDialogTitle("Carica scenario...");
		FileNameExtensionFilter filter = new FileNameExtensionFilter("File Scenario", "tis");
		fc.setFileFilter(filter);
		
		if (fc.showOpenDialog(this.gui) == JFileChooser.APPROVE_OPTION) {
			File selectedFile = fc.getSelectedFile();
			try {
				DataManager.load(selectedFile.toString());
				this.gui.refresh();
			} catch (IOException ex) {
				JOptionPane.showMessageDialog(this.gui, "Could not open scenario file", "Error", JOptionPane.ERROR_MESSAGE);
				ex.printStackTrace();
			}
		}
		this.gui.refresh();
	}
	
	public void onSaveScenario(ActionEvent e) {
		JFileChooser fc = new JFileChooser(System.getProperty("user.dir"));
		fc.setDialogTitle("Salva scenario...");
		FileNameExtensionFilter filter = new FileNameExtensionFilter("File Scenario", "tis");
		fc.setFileFilter(filter);
		
		if (fc.showSaveDialog(gui) == JFileChooser.APPROVE_OPTION) {
			File selectedFile = fc.getSelectedFile();
			try {
				DataManager.save(selectedFile.toString().endsWith(".tis") ? selectedFile.toString() : selectedFile.toString() + ".tis");
			} catch (IOException ex) {
				JOptionPane.showMessageDialog(this.gui, "Could not open scenario file", "Error", JOptionPane.ERROR_MESSAGE);
				ex.printStackTrace();
			}
		}
	}
	
	public void onPatientSelected(ListSelectionEvent listSelectionEvent) {
		if (listSelectionEvent.getValueIsAdjusting()) {
			return;
		}
		this.gui.updateInterface();
	}
	
	public void onAddPatientButton(ActionEvent e) {
		new NewPatientDialog().launch();
		this.gui.refresh();
	}
	
	public void onNewHospitalization(ActionEvent e) {
		if (DataManager.getHospitalizablePatients().isEmpty()) {
			JOptionPane.showMessageDialog(this.gui, "Non ci sono pazienti ricoverabili", "Errore", JOptionPane.WARNING_MESSAGE);
			return;
		}
		if (DataManager.getHospitalizedPatients().size() == 10) {
			JOptionPane.showMessageDialog(this.gui, "Massimo 10 pazienti possono essere ricoverati nello stesso momento", "Errore", JOptionPane.WARNING_MESSAGE);
			return;
		}
		new NewHospitalizationDialog().launch();
		this.gui.refresh();
	}
	
	public void onNewPrescription(ActionEvent e) {
		if (DataManager.getHospital().getMedicines().isEmpty()) {
			JOptionPane.showMessageDialog(this.gui, "Non ci sono farmaci nel sistema", "Errore", JOptionPane.WARNING_MESSAGE);
			return;
		}
		new NewPrescriptionDialog(this.gui.getSelectedPatient()).launch();
		this.gui.refresh();
	}
	
	public void onNewAdministration(ActionEvent e) {
		if (this.gui.getSelectedPatient().getCurrentHospitalization().getPrescriptions().isEmpty()) {
			JOptionPane.showMessageDialog(this.gui, "Il paziente selezionato non ha alcuna prescrizione", "Errore", JOptionPane.WARNING_MESSAGE);
			return;
		}
		new NewAdministrationDialog(this.gui.getSelectedPatient()).launch();
		this.gui.refresh();
	}
	
	public void onDismissPatient(ActionEvent e) {
		if (this.gui.getSelectedPatient().getCurrentHospitalization().getVitals().getAlarms().stream().anyMatch(Alarm::isUnresolved)) {
			JOptionPane.showMessageDialog(this.gui, "Il paziente selezionato ha degli allarmi non gestiti", "Errore", JOptionPane.ERROR_MESSAGE);
			return;
		}
		new DismissPatientDialog(this.gui.getSelectedPatient()).launch();
		this.gui.refresh();
	}
	
	public void onManageMedicines(ActionEvent e) {
		new ManageMedicinesDialog().launch();
	}
	
	public void onPrintReport(ActionEvent e) {
		if (DataManager.getPatients().stream().mapToLong(patient -> patient.getHospitalizations().size()).sum() == 0) {
			JOptionPane.showMessageDialog(this.gui, "Nessun report disponibile per la stampa", "Errore", JOptionPane.WARNING_MESSAGE);
			return;
		}
		new PrintReportDialog().launch();
	}
	
	public MouseListener onAlarmClick() {
		return new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				JAutoSuppliedList<AlarmDisplay> alarmsList = (JAutoSuppliedList<AlarmDisplay>) e.getSource();
				// Double-click
				if (e.getClickCount() == 2) {
					AlarmDisplay clickedAlarm = alarmsList.getModel().getElementAt(alarmsList.locationToIndex(e.getPoint()));
					String performedActions = JOptionPane.showInputDialog(gui, "Attività effettuate", "Gestione allarme", JOptionPane.PLAIN_MESSAGE);
					if (performedActions != null && !performedActions.equals("")) {
						clickedAlarm.stop();
						clickedAlarm.getAlarm().setPerformedActions(performedActions);
						gui.getActiveAlarms().remove(clickedAlarm);
					}
				}
			}
		};
	}
	
	public WindowListener onWindowClosing() {
		return new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				int confirmed = JOptionPane.showConfirmDialog(null,
						"Salvare lo scenario prima di chiudere", "Attenzione",
						JOptionPane.YES_NO_CANCEL_OPTION);
				
				if (confirmed == JOptionPane.CANCEL_OPTION) {
					return;
				}
				
				if (confirmed == JOptionPane.YES_OPTION) {
					onSaveScenario(null);
				}
				
				DataManager.setVariable("closing", true);
				gui.dispose();
			}
		};
	}
	
	public void onHospitalizationHistory(ActionEvent e) {
		if (DataManager.getPatients().stream().noneMatch(p -> p.getHospitalizations().stream().anyMatch(h -> h.getDismissalLetter() != null))) {
			JOptionPane.showMessageDialog(this.gui, "Nessun paziente è stato mai dimesso", "Errore", JOptionPane.WARNING_MESSAGE);
			return;
		}
		new HospitalizationHistoryDialog().launch();
	}
}
