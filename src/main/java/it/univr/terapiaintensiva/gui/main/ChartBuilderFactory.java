package it.univr.terapiaintensiva.gui.main;

import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.XYSeries;
import org.knowm.xchart.style.Styler;
import org.knowm.xchart.style.colors.XChartSeriesColors;
import org.knowm.xchart.style.markers.SeriesMarkers;

import java.util.Collections;
import java.util.Date;

public class ChartBuilderFactory {
	
	public static XYChart getPressureChart() {
		XYChart chart = new XYChartBuilder()
				.title("SBP & DBP")
				.yAxisTitle("mmHg")
				.theme(Styler.ChartTheme.Matlab)
				.build();
		
		chart.getStyler().setLegendPosition(Styler.LegendPosition.InsideNE);
		chart.getStyler().setDatePattern("HH:mm");
		chart.getStyler().setDefaultSeriesRenderStyle(XYSeries.XYSeriesRenderStyle.Line);
		
		Date now = new Date();
		
		XYSeries sbp = chart.addSeries("SBP", Collections.singletonList(now), Collections.singletonList(0));
		sbp.setMarker(SeriesMarkers.CIRCLE);
		sbp.setMarkerColor(XChartSeriesColors.BLUE);
		sbp.setLineColor(XChartSeriesColors.BLUE);
		sbp.setLineWidth(2.5F);
		sbp.setEnabled(false);
		
		XYSeries dbp = chart.addSeries("DBP", Collections.singletonList(now), Collections.singletonList(0));
		dbp.setMarker(SeriesMarkers.CIRCLE);
		dbp.setMarkerColor(XChartSeriesColors.GREEN);
		dbp.setLineColor(XChartSeriesColors.GREEN);
		dbp.setLineWidth(2.5F);
		dbp.setEnabled(false);
		
		return chart;
	}
	
	public static XYChart getHeartRateChart() {
		XYChart chart = new XYChartBuilder()
				.title("Battito Cardiaco")
				.yAxisTitle("bpm")
				.theme(Styler.ChartTheme.Matlab)
				.build();
		
		chart.getStyler().setLegendVisible(false);
		chart.getStyler().setDatePattern("HH:mm");
		chart.getStyler().setDefaultSeriesRenderStyle(XYSeries.XYSeriesRenderStyle.Line);
		
		XYSeries heartRate = chart.addSeries("HeartRate", Collections.singletonList(new Date()), Collections.singletonList(0));
		heartRate.setMarker(SeriesMarkers.CIRCLE);
		heartRate.setMarkerColor(XChartSeriesColors.RED);
		heartRate.setLineColor(XChartSeriesColors.RED);
		heartRate.setLineWidth(2.5F);
		heartRate.setEnabled(false);
		
		return chart;
	}
	
	public static XYChart getTemperatureChart() {
		XYChart chart = new XYChartBuilder()
				.title("Temperatura Corporea")
				.yAxisTitle("°C")
				.theme(Styler.ChartTheme.Matlab)
				.build();
		
		chart.getStyler().setLegendVisible(false);
		chart.getStyler().setDatePattern("HH:mm");
		chart.getStyler().setYAxisDecimalPattern("##.#");
		chart.getStyler().setDefaultSeriesRenderStyle(XYSeries.XYSeriesRenderStyle.Line);
		
		XYSeries temperature = chart.addSeries("Temperature", Collections.singletonList(new Date()), Collections.singletonList(0.0));
		temperature.setMarker(SeriesMarkers.CIRCLE);
		temperature.setMarkerColor(XChartSeriesColors.ORANGE);
		temperature.setLineColor(XChartSeriesColors.ORANGE);
		temperature.setLineWidth(2.5F);
		temperature.setEnabled(false);
		
		return chart;
	}
}
