package it.univr.terapiaintensiva.gui.main;

import it.univr.terapiaintensiva.monitoring.Vitals;
import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.XYChart;

import javax.swing.*;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class ChartUpdater extends SwingWorker<Boolean, Vitals.Reading<? extends Number>> {
	
	private final XChartPanel<XYChart> chartPanel;
	private final String seriesName;
	private List<? extends Vitals.Reading<? extends Number>> vitals;
	
	
	public ChartUpdater(XChartPanel<XYChart> chartPanel, String seriesName) {
		this.chartPanel = chartPanel;
		this.seriesName = seriesName;
	}
	
	// the Worker thread calls this
	@Override
	protected Boolean doInBackground() {
		
		Vitals.Reading<? extends Number>[] data;
		
		while (!isCancelled()) {
			if (vitals != null && vitals.size() != 0) {
				data = new Vitals.Reading<?>[vitals.size()];
				vitals.toArray(data);
				publish(data);
			}
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// when closing the app
				return false;
			}
		}
		
		return true;
	}
	
	// the EventDispatcher thread calls this
	@Override
	protected void process(List<Vitals.Reading<? extends Number>> chunks) {
		List<Date> xData = chunks.stream().map(r -> Date.from(r.getMoment().atZone(ZoneId.systemDefault()).toInstant())).collect(Collectors.toList());
		List<Number> yData = chunks.stream().map(Vitals.Reading::getValue).collect(Collectors.toList());
		chartPanel.getChart().updateXYSeries(seriesName, xData, yData, null);
		chartPanel.revalidate();
		chartPanel.repaint();
	}
	
	
	public void changeReadingSupplier(List<? extends Vitals.Reading<? extends Number>> reading) {
		this.vitals = reading;
		if (vitals != null) {
			this.chartPanel.getChart().getSeriesMap().get(seriesName).setEnabled(true);
		} else {
			this.chartPanel.getChart().getSeriesMap().get(seriesName).setEnabled(false);
		}
	}
}
