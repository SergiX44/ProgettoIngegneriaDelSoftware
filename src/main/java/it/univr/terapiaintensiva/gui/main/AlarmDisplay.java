package it.univr.terapiaintensiva.gui.main;

import com.alee.extended.time.ClockType;
import com.alee.extended.time.WebClock;
import it.univr.terapiaintensiva.monitoring.Alarm;
import it.univr.terapiaintensiva.persistence.users.Patient;

import java.util.Objects;

public class AlarmDisplay extends WebClock {
	
	private Patient owner;
	private Alarm alarm;
	
	AlarmDisplay(Patient owner, Alarm alarm) {
		this.owner = owner;
		this.alarm = alarm;
		
		setClockType(ClockType.timer);
		setTimeLeft(alarm.getType().getHandlingTime() * 60 * 1000);
		setTimePattern("mm:ss");
		addActionListener(e -> {
			AlarmDisplay source = (AlarmDisplay) e.getSource();
			source.setTimeLeft(0);
			source.stop();
		});
		start();
	}
	
	public Patient getOwner() {
		return owner;
	}
	
	public Alarm getAlarm() {
		return alarm;
	}
	
	
	@Override
	public boolean equals(Object other) {
		if (other instanceof AlarmDisplay) {
			AlarmDisplay otherAlarmDisplay = (AlarmDisplay) other;
			return otherAlarmDisplay.owner == this.owner && otherAlarmDisplay.alarm == this.alarm;
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(owner, alarm);
	}
	
	@Override
	public String toString() {
		return "<" + getText() + "> " + this.owner.toString() + ": " + this.alarm.toString();
	}
	
}
