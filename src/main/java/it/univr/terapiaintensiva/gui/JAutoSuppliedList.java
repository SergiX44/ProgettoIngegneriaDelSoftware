package it.univr.terapiaintensiva.gui;

import javax.swing.*;
import java.util.List;
import java.util.function.Supplier;

public class JAutoSuppliedList<E> extends JList<E> {
	
	private DefaultListModel<E> listModel = new DefaultListModel<>();
	private Supplier<List<E>> supplier;
	
	
	public JAutoSuppliedList(Supplier<List<E>> supplier) {
		super();
		this.supplier = supplier;
	}
	
	
	public void rebuild() {
		this.listModel = new DefaultListModel<>();
		supplier.get().forEach(listModel::addElement);
		setModel(listModel);
	}
	
	public void clear() {
		this.listModel = new DefaultListModel<>();
		setModel(listModel);
	}
}
