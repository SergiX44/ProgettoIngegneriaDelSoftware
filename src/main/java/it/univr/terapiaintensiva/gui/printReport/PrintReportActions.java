package it.univr.terapiaintensiva.gui.printReport;

import it.univr.terapiaintensiva.gui.GUIActions;
import it.univr.terapiaintensiva.persistence.DataManager;
import it.univr.terapiaintensiva.persistence.hospitalization.Hospitalization;
import it.univr.terapiaintensiva.persistence.users.Patient;
import org.commonmark.ext.gfm.tables.TablesExtension;
import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.print.PrinterException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PrintReportActions extends GUIActions<PrintReportDialog> {
	
	public PrintReportActions(PrintReportDialog gui) {
		super(gui);
	}
	
	public String getReport(Map<Patient, List<Hospitalization>> hospitalizationMap, LocalDate selectedWeek) {
		StringBuilder markdown = new StringBuilder();
		
		markdown.append("# Report Terapia Intensiva\n");
		markdown.append("## Settimana ").append(selectedWeek.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		markdown.append("\n\n");
		
		markdown.append("| Paziente | Data ricovero | Prescrizioni | Somministrazioni | Allarmi generati |").append("\n");
		markdown.append("| --- | :---: | :---: | :---: | :---: |").append("\n");
		
		hospitalizationMap.forEach((patient, hospitalizations) -> markdown.append("| ")
				.append(patient)
				.append(" | ")
				.append(hospitalizations.stream().map(h -> h.getDate().format(DateTimeFormatter.ofPattern("dd/MM/yyy"))).collect(Collectors.toList()).toString())
				.append(" | ")
				.append(hospitalizations.stream().mapToInt(h -> h.getPrescriptions().size()).sum())
				.append(" | ")
				.append(hospitalizations.stream().mapToInt(h -> h.getAdministrations().size()).sum())
				.append(" | ")
				.append(hospitalizations.stream().mapToInt(h -> h.getVitals().getAlarms().size()).sum())
				.append(" |")
				.append("\n")
		);
		
		markdown.append("\n").append("*Medico Primario:* ").append(DataManager.getCurrentUser().getFirstName()).append(" ").append(DataManager.getCurrentUser().getLastName());
		
		Node parsedMarkdown = Parser.builder().extensions(Collections.singletonList(TablesExtension.create())).build().parse(markdown.toString());
		return HtmlRenderer.builder().extensions(Collections.singletonList(TablesExtension.create())).build().render(parsedMarkdown);
	}
	
	public void onWeekChange(ActionEvent e) {
		LocalDate selectedWeek = this.gui.getSelectedWeek();
		
		Map<Patient, List<Hospitalization>> hospitalizationsMap = new HashMap<>();
		DataManager.getPatients().forEach(patient -> {
			
			List<Hospitalization> patientHospitalizations = patient.getHospitalizations().stream()
					.filter(hospitalization -> hospitalization.getDate().with(DayOfWeek.MONDAY).isEqual(selectedWeek))
					.collect(Collectors.toList());
			if (!patientHospitalizations.isEmpty()) {
				hospitalizationsMap.put(patient, patient.getHospitalizations());
			}
			
		});
		
		String report = this.getReport(hospitalizationsMap, selectedWeek);
		
		this.gui.getReportPane().setText(report);
	}
	
	public void onPrintButton(ActionEvent e) {
		if (this.gui.getSelectedWeek() == null) {
			JOptionPane.showMessageDialog(this.gui, "Selezionare una settimana", "Attenzione", JOptionPane.WARNING_MESSAGE);
			return;
		}
		try {
			if (this.gui.getReportPane().print()) {
				onCancelButton(null);
			}
		} catch (PrinterException ex) {
			JOptionPane.showMessageDialog(this.gui, "Impossibile stampare il report: " + ex, "Errore", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public void onCancelButton(ActionEvent e) {
		this.gui.dispose();
	}
}
