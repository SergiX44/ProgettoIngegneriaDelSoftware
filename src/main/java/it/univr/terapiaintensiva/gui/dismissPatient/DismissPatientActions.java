package it.univr.terapiaintensiva.gui.dismissPatient;

import it.univr.terapiaintensiva.gui.GUIActions;
import it.univr.terapiaintensiva.monitoring.Alarm;
import it.univr.terapiaintensiva.persistence.DataManager;

import java.awt.event.ActionEvent;

public class DismissPatientActions extends GUIActions<DismissPatientDialog> {
	
	
	public DismissPatientActions(DismissPatientDialog dismissPatientGUI) {
		super(dismissPatientGUI);
	}
	
	
	public void onDismissButton(ActionEvent e) {
		String letter = this.gui.getLetter();
		if (letter == null || letter.equals("")) {
			this.gui.alertMessage("Inserire la lettera di dimissioni del paziente");
			return;
		}

		if (this.gui.getPatient().getCurrentHospitalization().getVitals().getAlarms().stream().anyMatch(Alarm::isUnresolved)) {
			this.gui.alertMessage("Il paziente selezionato ha degli allarmi non gestiti");
			return;
		}
		
		DataManager.getHospital().dismiss(this.gui.getPatient(), letter);
		
		onCancelButton(null);
	}
	
	public void onCancelButton(ActionEvent e) {
		this.gui.dispose();
	}
	
}
