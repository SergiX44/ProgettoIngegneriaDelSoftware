package it.univr.terapiaintensiva.gui.newHospitalization;

import it.univr.terapiaintensiva.gui.GUIActions;
import it.univr.terapiaintensiva.persistence.DataManager;
import it.univr.terapiaintensiva.persistence.users.Patient;

import java.awt.event.ActionEvent;

public class NewHospitalizationActions extends GUIActions<NewHospitalizationDialog> {
	
	
	public NewHospitalizationActions(NewHospitalizationDialog newHospitalizationDialog) {
		super(newHospitalizationDialog);
	}


    public void onSaveButton(ActionEvent e) {
        Patient patient = this.gui.getSelectedPatient();
        if (patient == null) {
	        this.gui.alertMessage("Selezionare un paziente da ricoverare");
            return;
        }
        String diagnosis = this.gui.getDiagnosis();
        if (diagnosis == null || diagnosis.equals("")) {
	        this.gui.alertMessage("Aggiungere una diagnosi per il paziente");
            return;
        }
	
		DataManager.getHospital().hospitalize(patient, diagnosis);
	
	    onCancelButton(null);
    }
	
	public void onCancelButton(ActionEvent e) {
		this.gui.dispose();
    }
	
}
