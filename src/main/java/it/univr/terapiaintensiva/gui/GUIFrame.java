package it.univr.terapiaintensiva.gui;

import javax.swing.*;
import java.awt.*;

/**
 * A generic gui
 */
public abstract class GUIFrame<Actions extends GUIActions<? extends GUIFrame<Actions>>> extends JFrame implements GUI<Actions> {
	
	protected Actions actions;
	
	public GUIFrame(String title, int defaultCloseOperation) {
		setTitle(title);
		setDefaultCloseOperation(defaultCloseOperation);
	}
	
	public GUIFrame(String title) {
		this(title, DISPOSE_ON_CLOSE);
	}
	
	@Override
	public void launch() {
		this.pack();
		this.alignCenter();
		this.setVisible(true);
	}
	
	@Override
	public void alignCenter() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(
				screenSize.width / 2 - this.getSize().width / 2,
				screenSize.height / 2 - this.getSize().height / 2
		);
	}
	
	@Override
	public void refresh() {
		// do nothing here
	}
}
