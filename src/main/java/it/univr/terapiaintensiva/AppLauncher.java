package it.univr.terapiaintensiva;

import com.alee.laf.WebLookAndFeel;
import it.univr.terapiaintensiva.demo.cheats.VitalsCheatsGUI;
import it.univr.terapiaintensiva.gui.login.LoginGUI;
import it.univr.terapiaintensiva.persistence.DataManager;

import javax.swing.*;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

public class AppLauncher {
	
	private static final Logger log = Logger.getLogger(AppLauncher.class.getName());
	
	public static void main(String[] args) {

		// *** LOAD GUI ***
		WebLookAndFeel.install();
		WebLookAndFeel.setDecorateAllWindows(true);


		// *** HANDLE ARGUMENTS ***
		List<String> argsList = Arrays.asList(args);
		
		// demo mode
		if (argsList.contains("-demo")) {
			DataManager.setupDemo();
		} else {
			// only demo mode is supported for now
			JOptionPane.showMessageDialog(null, "Al momento è disponibile solo la modalità demo", "Only demo mode available", JOptionPane.ERROR_MESSAGE);
			log.severe("Real-world scenario has not been implemented yet, only demo mode is available, exiting...");
			System.exit(-1);
			
			// load persistence from the eventually specified path
			if (argsList.contains("-file")) {
				String path = null;
				int filepathIndex = argsList.indexOf("-file") + 1;
				if (filepathIndex >= argsList.size() || (path = argsList.get(filepathIndex)).startsWith("-")) {
					// specified an invalid filepath
					JOptionPane.showMessageDialog(null, "Il percorso specificato non è valido", "Invalid filepath specified", JOptionPane.ERROR_MESSAGE);
					log.severe("An invalid path has been specified, exiting...");
					System.exit(-1);
				}
				try {
					DataManager.load(path);
				} catch (IOException e) {
					JOptionPane.showMessageDialog(null, "Impossibile caricare il database indicato", "Unable to load the specified database", JOptionPane.ERROR_MESSAGE);
					log.severe("Cannot load persistence data from the specified path, exiting...");
					System.exit(-1);
				}
			} else {
				// load persistence from default path
				try {
					DataManager.loadDefault();
				} catch (IOException e) {
					JOptionPane.showMessageDialog(null, "Impossibile caricare il database di default", "Unable to load the default database", JOptionPane.ERROR_MESSAGE);
					log.severe("Cannot load the default persistence data, exiting...");
					System.exit(-1);
				}
			}
		}


		// *** LAUNCH GUI ***
		SwingUtilities.invokeLater(() -> {
			LoginGUI loginGUI = new LoginGUI();
			if (argsList.contains("-demo")) {
				loginGUI.setMedID("PRIMARY-0000");
				loginGUI.setPassword("primary");
				VitalsCheatsGUI cheats = VitalsCheatsGUI.getInstance();
				cheats.launch();
				cheats.toFront();
			}
			loginGUI.launch();
		});
	
	}
}
