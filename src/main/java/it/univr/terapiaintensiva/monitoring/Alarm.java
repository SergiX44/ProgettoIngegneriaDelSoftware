package it.univr.terapiaintensiva.monitoring;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Alarm implements Serializable {
	
	private AlarmType type;
	private LocalDateTime fireTime;
	private String performedActions;
	
	public Alarm(AlarmType type) {
		this.type = type;
		this.fireTime = LocalDateTime.now();
	}
	
	public AlarmType getType() {
		return type;
	}
	
	public void setType(AlarmType type) {
		this.type = type;
	}
	
	public LocalDateTime getFireTime() {
		return fireTime;
	}
	
	public void setFireTime(LocalDateTime fireTime) {
		this.fireTime = fireTime;
	}
	
	public String getPerformedActions() {
		return performedActions;
	}
	
	public void setPerformedActions(String performedActions) {
		this.performedActions = performedActions;
	}
	
	public boolean isUnresolved() {
		return performedActions == null;
	}
	
	
	@Override
	public String toString() {
		return type.toString();
	}
}
