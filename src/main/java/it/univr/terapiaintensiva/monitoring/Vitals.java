package it.univr.terapiaintensiva.monitoring;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.LinkedList;

public class Vitals implements Serializable {
	
	private LinkedList<SBPReading> sbp = new LinkedList<>();
	private LinkedList<DBPReading> dbp = new LinkedList<>();
	private LinkedList<HeartRateReading> heartRate = new LinkedList<>();
	private LinkedList<TemperatureReading> temperature = new LinkedList<>();
	private LinkedList<Alarm> alarms = new LinkedList<>();
	
	
	public Vitals() {
    }
	
	
	public LinkedList<SBPReading> getSBP() {
		return sbp;
	}
	
	public LinkedList<DBPReading> getDBP() {
		return dbp;
	}
	
	public LinkedList<HeartRateReading> getHeartRate() {
		return heartRate;
	}
	
	public LinkedList<TemperatureReading> getTemperature() {
		return temperature;
	}
	
	public LinkedList<Alarm> getAlarms() {
		return alarms;
	}
	
	
	public void addSBPReading(int value) {
		this.sbp.add(new SBPReading(LocalDateTime.now(), value));
	}
	
	public void addDBPReading(int value) {
		this.dbp.add(new DBPReading(LocalDateTime.now(), value));
	}
	
	public void addHeartRateReading(int value) {
		this.heartRate.add(new HeartRateReading(LocalDateTime.now(), value));
	}
	
	public void addTemperatureReading(double value) {
		this.temperature.add(new TemperatureReading(LocalDateTime.now(), value));
	}
	
	
	
	// *** READING STRUCTURE ***
	
	public abstract class Reading<T extends Number> implements Serializable {
		
		private final T value;
		private final LocalDateTime time;
		
		public Reading(T value, LocalDateTime time) {
			this.value = value;
			this.time = time;
		}
		
		public T getValue() {
			return value;
		}
		
		public LocalDateTime getMoment() {
			return time;
		}
	}
	
	public final class SBPReading extends Reading<Integer> {
		public SBPReading(LocalDateTime readingTime, Integer sbp) {
			super(sbp, readingTime);
		}
	}
	
	public final class DBPReading extends Reading<Integer> {
		public DBPReading(LocalDateTime readingTime, Integer dbp) {
			super(dbp, readingTime);
		}
	}
	
	public final class HeartRateReading extends Reading<Integer> {
		public HeartRateReading(LocalDateTime readingTime, Integer heartRate) {
			super(heartRate, readingTime);
		}
	}
	
	public final class TemperatureReading extends Reading<Double> {
		public TemperatureReading(LocalDateTime readingTime, Double temperature) {
			super(temperature, readingTime);
		}
	}
	
	@Override
	public String toString() {
		return "Current vitals : \n" +
				"\tsbp = " + (sbp.isEmpty() ? 0 : sbp.getLast().getValue()) + "\n" +
				"\tdbp = " + (dbp.isEmpty() ? 0 : dbp.getLast().getValue()) + "\n" +
				"\theartRate = " + (heartRate.isEmpty() ? 0 : heartRate.getLast().getValue()) + "\n" +
				"\ttemperature = " + (temperature.isEmpty() ? 0 : temperature.getLast().getValue()) + "\n" +
				"\talarms = " + Arrays.toString(alarms.toArray());
	}
}
