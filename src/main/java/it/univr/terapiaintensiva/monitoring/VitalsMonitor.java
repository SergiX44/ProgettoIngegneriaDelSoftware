package it.univr.terapiaintensiva.monitoring;

import it.univr.terapiaintensiva.persistence.users.Patient;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

public class VitalsMonitor {
	
	// defined in seconds
	private static final int ALARMS_UPDATE_INTERVAL = 1;
	// defined in minutes
	private static final int SBP_UPDATE_INTERVAL = 2;
	private static final int DBP_UPDATE_INTERVAL = 2;
	private static final int HEART_RATE_UPDATE_INTERVAL = 5;
	private static final int TEMPERATURE_UPDATE_INTERVAL = 3;
	private static final int RESIZE_DATA_INTERVAL = 120;
	
	private final Constructor<? extends VitalsMonitorWorker> workerConstructor;
	
	private Map<Patient, VitalsMonitorWorker> patientsList = new HashMap<>();
	private ScheduledExecutorService stopMonitoringThread = Executors.newSingleThreadScheduledExecutor();
	private Map<Patient, ScheduledFuture<?>> scheduledMonitorsStop = new HashMap<>();
	
	
	public VitalsMonitor(Class<? extends VitalsMonitorWorker> workerImpl) {
		try {
			this.workerConstructor = workerImpl.getConstructor(Vitals.class);
		} catch (NoSuchMethodException e) {
			// should never reach this point
			throw new NoSuchMethodError("Bad implementation provided");
		}
		if (!Modifier.isPublic(workerConstructor.getModifiers())) {
			throw new NoSuchMethodError("Constructor needs to be public");
		}
	}
	
	
	public void addPatient(Patient patient) throws InvocationTargetException {
		if (scheduledMonitorsStop.containsKey(patient)) {
			scheduledMonitorsStop.get(patient).cancel(true);
		} else {
			VitalsMonitorWorker vitalsMonitorWorker;
			try {
				vitalsMonitorWorker = workerConstructor.newInstance(patient.getCurrentHospitalization().getVitals());
			} catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
				throw new InvocationTargetException(e);
			}
			patientsList.put(patient, vitalsMonitorWorker);
		}
	}
	
	public void removePatient(Patient patient) {
		scheduledMonitorsStop.put(patient, stopMonitoringThread.schedule(() -> this.stopMonitoring(patient), 15, TimeUnit.MINUTES));
	}
	
	public void stopMonitoring(Patient patient) {
		scheduledMonitorsStop.remove(patient);
		patientsList.remove(patient).stopMonitoring();
	}
	
	public void shutdown() {
		// works if closing from MainGUI and LoginGUI in the 15 minutes after the logout
		stopMonitoringThread.shutdownNow();
		scheduledMonitorsStop.clear();
		patientsList.forEach((p, w) -> w.stopMonitoring());
		patientsList.clear();
		// after 15 minutes from the logout every worker has been stopped already, so no more operation is needed
	}
	
	
	public static abstract class VitalsMonitorWorker {
		
		// systolic blood pressure : 110 ≤ SBP ≤ 140 mmHg
		public static final int SBP_NORMAL_MAX = 140;
		public static final int SBP_NORMAL = 125;
		public static final int SBP_NORMAL_MIN = 110;
		// diastolic blood pressure : 60 ≤ DBP ≤  90 mmHg
		public static final int DBP_NORMAL_MAX = 90;
		public static final int DBP_NORMAL = 75;
		public static final int DBP_NORMAL_MIN = 60;
		// heart rate : 60 ≤ rate ≤ 100 bpm
		public static final int HEARTH_RATE_NORMAL_MAX = 100;
		public static final int HEARTH_RATE_NORMAL = 80;
		public static final int HEARTH_RATE_NORMAL_MIN = 60;
		// temperature : 37±0.4 °C
		public static final double TEMPERATURE_NORMAL_MAX = 37.4f;
		public static final double TEMPERATURE_NORMAL = 37.0f;
		public static final double TEMPERATURE_NORMAL_MIN = 36.6f;
		// thresholds
		public static final BiPredicate<Integer, Integer> HYPERTENSION_THRESHOLD = (sbp, dbp) -> sbp > 140 && dbp > 90;
		public static final BiPredicate<Integer, Integer> HYPOTENSION_THRESHOLD = (sbp, dbp) -> sbp < 100 || dbp < 60;
		public static final Predicate<Integer> TACHYCARDIA_THRESHOLD = (bpm) -> bpm > 100;
		public static final Predicate<Integer> FLUTTER_THRESHOLD = (bpm) -> bpm > 150;
		public static final Predicate<Integer> VENTRICULAR_FIBRILLATION_THRESHOLD = (bpm) -> bpm > 350;
		public static final Predicate<Double> HYPERTHERMIA_THRESHOLD = (t) -> t > 39f;
		public static final Predicate<Double> HYPOTHERMIA_THRESHOLD = (t) -> t < 35f;
		
		
		private final ScheduledExecutorService monitoringThread = Executors.newSingleThreadScheduledExecutor();
		
		protected final Vitals patientVitals;
		
		
		protected VitalsMonitorWorker(Vitals patientVitals) {
			this.patientVitals = patientVitals;
			monitoringThread.scheduleAtFixedRate(this::updateSBP, 0, SBP_UPDATE_INTERVAL, TimeUnit.MINUTES);
			monitoringThread.scheduleAtFixedRate(this::updateDBP, 0, DBP_UPDATE_INTERVAL, TimeUnit.MINUTES);
			monitoringThread.scheduleAtFixedRate(this::updateHeartRate, 0, HEART_RATE_UPDATE_INTERVAL, TimeUnit.MINUTES);
			monitoringThread.scheduleAtFixedRate(this::updateTemperature, 0, TEMPERATURE_UPDATE_INTERVAL, TimeUnit.MINUTES);
			monitoringThread.scheduleWithFixedDelay(this::updateAlarms, 0, ALARMS_UPDATE_INTERVAL, TimeUnit.SECONDS);
		}
		
		
		private void updateSBP() {
			patientVitals.addSBPReading(systolicBloodPressure());
			// clean data older than 2 hours
			if (patientVitals.getSBP().getFirst().getMoment().isBefore(LocalDateTime.now().minusMinutes(RESIZE_DATA_INTERVAL))) {
				patientVitals.getSBP().poll();
			}
		}
		
		private void updateDBP() {
			patientVitals.addDBPReading(diastolicBloodPressure());
			// clean data older than 2 hours
			if (patientVitals.getDBP().getFirst().getMoment().isBefore(LocalDateTime.now().minusMinutes(RESIZE_DATA_INTERVAL))) {
				patientVitals.getDBP().poll();
			}
		}
		
		private void updateHeartRate() {
			patientVitals.addHeartRateReading(heartRate());
			// clean data older than 2 hours
			if (patientVitals.getHeartRate().getFirst().getMoment().isBefore(LocalDateTime.now().minusMinutes(RESIZE_DATA_INTERVAL))) {
				patientVitals.getHeartRate().poll();
			}
		}
		
		private void updateTemperature() {
			patientVitals.addTemperatureReading(temperature());
			// clean data older than 2 hours
			if (patientVitals.getTemperature().getFirst().getMoment().isBefore(LocalDateTime.now().minusMinutes(RESIZE_DATA_INTERVAL))) {
				patientVitals.getTemperature().poll();
			}
		}
		
		private void updateAlarms() {
			alarms().stream().filter(alarm -> !patientVitals.getAlarms().contains(alarm)).forEach(alarm -> patientVitals.getAlarms().add(alarm));
		}
		
		
		private void stopMonitoring() {
			monitoringThread.shutdownNow();
		}
		
		
		protected abstract int systolicBloodPressure();
		
		protected abstract int diastolicBloodPressure();
		
		protected abstract int heartRate();
		
		protected abstract double temperature();
		
		protected abstract List<Alarm> alarms();
		
	}
}
