package it.univr.terapiaintensiva.monitoring;

import java.io.Serializable;

public enum AlarmType implements Serializable {
	
	ARRHYTHMIA("Aritmia", 1, 3),
	TACHYCARDIA("Tachicardia", 1, 3),
	FLUTTER("Flutter", 3, 1),
	VENTRICULAR_FIBRILLATION("Fibrillazione ventricolare", 3, 1),
	HYPERTENSION("Ipertensione", 2, 2),
	HYPOTENSION("Ipotensione", 2, 2),
	HYPERTHERMIA("Ipertermia", 2, 2),
	HYPOTHERMIA("Ipotermia", 2, 2);
	
	
	private final String alarmName;
	private final int severityLevel;
	private final int handlingTime;
	
	AlarmType(String alarmName, int severityLevel, int handlingTime) {
		this.alarmName = alarmName;
		this.severityLevel = severityLevel;
		this.handlingTime = handlingTime;
	}
	
	public String getAlarmTypeName() {
		return alarmName;
	}
	
	public int getSeverityLevel() {
		return severityLevel;
	}
	
	public int getHandlingTime() {
		return handlingTime;
	}
	
	@Override
	public String toString() {
		return getAlarmTypeName() + " (" + severityLevel + ")";
	}
}