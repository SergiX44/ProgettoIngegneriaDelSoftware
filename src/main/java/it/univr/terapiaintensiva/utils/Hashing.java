package it.univr.terapiaintensiva.utils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Logger;

public class Hashing {
	
	public static final Logger log = Logger.getLogger(Hashing.class.getName());
	
	public static String sha512(String message) {
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-512");
			byte[] hash = digest.digest(message.getBytes(StandardCharsets.UTF_8));
			StringBuilder hashString = new StringBuilder();
			for (byte b : hash) {
				hashString.append(String.format("%02x", b));
			}
			return hashString.toString().toUpperCase();
		} catch (NoSuchAlgorithmException e) {
			log.severe("No SHA-512 algorithm found in this JVM");
		}
		return null;
	}
}
