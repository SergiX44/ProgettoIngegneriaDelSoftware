package it.univr.terapiaintensiva.persistence.hospitalization;

import java.io.Serializable;

public class Medicine implements Serializable {

    private String commercialName;
	private boolean notUsedAnymore;
	
	public Medicine(String commercialName) {
		this.commercialName = commercialName;
		this.notUsedAnymore = false;
	}
	
	public String getCommercialName() {
		return commercialName;
	}
	
	public Medicine setCommercialName(String commercialName) {
		this.commercialName = commercialName;
		return this;
	}
	
	public boolean isInUse() {
		return !notUsedAnymore;
	}
	
	public void setNotUsedAnymore() {
		this.notUsedAnymore = true;
	}
	
	
	@Override
	public String toString() {
		return commercialName;
	}
}
