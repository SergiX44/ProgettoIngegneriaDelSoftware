package it.univr.terapiaintensiva.persistence.hospitalization;

import java.io.Serializable;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Administration implements Serializable {
	
	private Prescription prescription;
	private LocalDateTime time;
	private double dose;
	private String notes;
	
	public Administration(Prescription prescription, LocalDateTime time, double dose) {
		this.prescription = prescription;
		this.time = time;
		this.dose = dose;
	}
	
	public Prescription getPrescription() {
		return prescription;
	}
	
	public Administration setPrescription(Prescription prescription) {
		this.prescription = prescription;
		return this;
	}
	
	public LocalDateTime getTime() {
		return time;
	}
	
	public Administration setTime(LocalDateTime time) {
		this.time = time;
		return this;
	}
	
	public double getDose() {
		return dose;
	}
	
	public Administration setDose(double dose) {
		this.dose = dose;
		return this;
	}
	
	public String getNotes() {
		return notes;
	}
	
	public Administration setNotes(String notes) {
		this.notes = notes;
		return this;
	}
	
	
	@Override
	public String toString() {
		return MessageFormat.format("{0} {1} di {2}, {3}",
				this.dose,
				prescription.getQuantityUnit(),
				prescription.getMedicine().toString(),
				this.time.format(DateTimeFormatter.ofPattern("HH:mm dd/MM/yyyy"))
		);
	}
}
