package it.univr.terapiaintensiva.persistence.hospitalization;

import it.univr.terapiaintensiva.monitoring.Vitals;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Hospitalization implements Serializable {

	private String diagnosis;
	private Vitals vitals = new Vitals();
	private List<Prescription> prescriptions = new ArrayList<>();
	private List<Administration> administrations = new ArrayList<>();
	private String dismissalLetter;
	private LocalDate date;


	public Hospitalization(String diagnosis) {
		this.diagnosis = diagnosis;
		this.date = LocalDate.now();
	}


	public LocalDate getDate() {
		return date;
	}

	public String getDiagnosis() {
		return diagnosis;
	}

	public Hospitalization setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
		return this;
	}

	public List<Prescription> getPrescriptions() {
		return prescriptions;
	}

	public Hospitalization setPrescriptions(List<Prescription> prescriptions) {
		this.prescriptions = prescriptions;
		return this;
	}

	public List<Administration> getAdministrations() {
		return administrations;
	}

	public Hospitalization setAdministrations(List<Administration> administrations) {
		this.administrations = administrations;
		return this;
	}

	public Vitals getVitals() {
		return vitals;
	}

	public Hospitalization setVitals(Vitals vitals) {
		this.vitals = vitals;
		return this;
	}

	public String getDismissalLetter() {
		return dismissalLetter;
	}

	public Hospitalization setDismissalLetter(String dismissalLetter) {
		this.dismissalLetter = dismissalLetter;
		return this;
	}


	@Override
	public String toString() {
		return date.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
	}
}
