package it.univr.terapiaintensiva.persistence.hospitalization;

import it.univr.terapiaintensiva.persistence.users.Doctor;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class Prescription implements Serializable {
	
	private Doctor doctor;
    private Medicine medicine;
    private LocalDate beginningDate;
    private LocalDate endingDate;
    private int dailyDoses;
	private List<Double> doseQuantity;
	private String quantityUnit;
	
	public Prescription(Doctor doctor, Medicine medicine, LocalDate beginningDate, LocalDate endingDate, int dailyDoses, List<Double> doseQuantity, String quantityUnit) {
		this.doctor = doctor;
		this.medicine = medicine;
		this.beginningDate = beginningDate;
		this.endingDate = endingDate;
		this.dailyDoses = dailyDoses;
		this.doseQuantity = doseQuantity;
		this.quantityUnit = quantityUnit;
	}
	
	public Doctor getDoctor() {
		return doctor;
	}
	
	public Prescription setDoctor(Doctor doctor) {
		this.doctor = doctor;
		return this;
	}
	
	public Medicine getMedicine() {
		return medicine;
	}
	
	public Prescription setMedicine(Medicine medicine) {
		this.medicine = medicine;
		return this;
	}
	
	public LocalDate getBeginningDate() {
		return beginningDate;
	}
	
	public Prescription setBeginningDate(LocalDate beginningDate) {
		this.beginningDate = beginningDate;
		return this;
	}
	
	public LocalDate getEndingDate() {
		return endingDate;
	}
	
	public Prescription setEndingDate(LocalDate endingDate) {
		this.endingDate = endingDate;
		return this;
	}
	
	public int getDailyDoses() {
		return dailyDoses;
	}
	
	public Prescription setDailyDoses(int dailyDoses) {
		this.dailyDoses = dailyDoses;
		return this;
	}
	
	public List<Double> getDoseQuantity() {
		return doseQuantity;
	}
	
	public Prescription setDoseQuantity(List<Double> doseQuantity) {
		this.doseQuantity = doseQuantity;
		return this;
	}
	
	public String getQuantityUnit() {
		return quantityUnit;
	}
	
	public Prescription setQuantityUnit(String quantityUnit) {
		this.quantityUnit = quantityUnit;
		return this;
	}
	
	
	@Override
	public String toString() {
		return medicine.toString() + ", " + dailyDoses + " al giorno (" + String.join(", ", doseQuantity.stream().map(String::valueOf).collect(Collectors.toList())) + " " + quantityUnit + ")";
	}
}
