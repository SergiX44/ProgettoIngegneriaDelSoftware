package it.univr.terapiaintensiva.persistence;


import it.univr.terapiaintensiva.demo.DemoDataGenerator;
import it.univr.terapiaintensiva.demo.cheats.VitalsCheatsGUI;
import it.univr.terapiaintensiva.monitoring.VitalsMonitor;
import it.univr.terapiaintensiva.persistence.users.NursingStaff;
import it.univr.terapiaintensiva.persistence.users.Patient;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.function.Predicate;
import java.util.logging.Logger;
import java.util.stream.Stream;

public class DataManager {
	
	private static final Logger log = Logger.getLogger(DataManager.class.getName());
	
	private static final String DEFAULT_PATH = "./";
	private static final String DEFAULT_FILENAME = "terapiaIntensiva.tis";
	private static final String DEMO_FILENAME = "demo.tis";
	
	private static Hospital hospital;
	private static VitalsMonitor monitor;
	private static NursingStaff currentUser;
	private static SharedVariables variables = new SharedVariables();
	
	
	private DataManager() {
	}
	
	public static void save(String path) throws IOException {
		
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path))) {
			oos.writeObject(hospital);
		} catch (IOException e) {
			log.severe("Error saving persistence data: " + e);
			throw e;
		}
	}
	
	public static void load(String path) throws IOException {
		File f = new File(path);
		
		if (f.exists()) {
			try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f))) {
				hospital = (Hospital) ois.readObject();
			} catch (IOException | ClassNotFoundException e) {
				log.severe("Error loading persistence data: " + e);
				throw new IOException(e);
			}
		} else {
			throw new FileNotFoundException();
		}
	}
	
	public static void saveDefault() throws IOException {
		save(Paths.get(DEFAULT_PATH, DEFAULT_FILENAME).toString());
	}
	
	public static void loadDefault() throws IOException {
		try {
			load(Paths.get(DEFAULT_PATH, DEFAULT_FILENAME).toString());
		} catch (IOException e) {
			log.severe("Cannot load persistence data from the default path: " + e);
			throw new IOException(e);
		}
	}
	
	public static void setupDemo() {
		String demoPath = Paths.get(DEFAULT_PATH, DEMO_FILENAME).toString();
		File demoDB = new File(demoPath);
		// performs "demo mode" initialization
		if (!demoDB.exists()) {
			hospital = new Hospital();
			addNursingStaff(DemoDataGenerator.mockPrimaryDoctor());
			addNursingStaff(DemoDataGenerator.mockDoctor());
			addNursingStaff(DemoDataGenerator.mockNurse());
			getPatients().addAll(DemoDataGenerator.mockPatients());
			try {
				save(demoPath);
			} catch (IOException e) {
				log.severe("Cannot save persistence data to the demo path");
				System.exit(-1);
			}
		}
		// load demo scenario
		try {
			load(demoPath);
		} catch (IOException e) {
			log.severe("Invalid demo scenario, reinitializing...");
			if (demoDB.delete()){
				setupDemo();
			} else {
				throw new ExceptionInInitializerError("Cannot reinitialize the demo scenario");
			}
		}
		variables.addObserver(VitalsCheatsGUI.getInstance());
		monitor = new VitalsMonitor(DemoDataGenerator.class);
	}
	
	public static void startMonitor() {
		for (Patient p : getHospitalizedPatients()) {
			try {
				monitor.addPatient(p);
			} catch (InvocationTargetException e) {
				log.severe("Could not resume patient's data generation with this monitor");
			}
		}
	}
	
	public static void scheduleStopMonitor() {
		DataManager.getHospitalizedPatients().forEach(p -> DataManager.getMonitor().removePatient(p));
	}
	
	private static void stopMonitor() {
		DataManager.getMonitor().shutdown();
	}
	
	
	public static class SharedVariables extends Observable {
		private Map<String, Object> values = new HashMap<>();
		
		@Override
		protected synchronized void setChanged() {
			super.setChanged();
		}
	}
	
	public static void setVariable(String varName, Object varValue) {
		variables.values.put(varName, varValue);
		variables.setChanged();
		variables.notifyObservers();
		if (varName.equals("closing")) {
			DataManager.stopMonitor();
		}
	}
	
	
	public static Object getVariable(String varName) {
		return variables.values.get(varName);
	}
	
	
	// *** DB-LIKE METHODS ***
	
	
	public static VitalsMonitor getMonitor() {
		return monitor;
	}
	
	public static void setMonitor(VitalsMonitor monitor) {
		DataManager.monitor = monitor;
	}
	
	public static NursingStaff getCurrentUser() {
		return currentUser;
	}
	
	public static void setCurrentUser(NursingStaff user) {
		currentUser = user;
	}
	
	
	public static Hospital getHospital() {
		return hospital;
	}
	
	
	public static List<Patient> getPatients() {
		return hospital.getPatients();
	}
	
	public static void addPatient(Patient patient) {
		hospital.getPatients().add(patient);
	}
	
	public static Stream<Patient> searchPatient(Predicate<Patient> condition) {
		return hospital.getPatients().stream().filter(condition);
	}
	
	
	public static List<NursingStaff> getNursingStaff() {
		return hospital.getHospitalStaff();
	}
	
	public static void addNursingStaff(NursingStaff staff) {
		hospital.getHospitalStaff().add(staff);
	}
	
	public static Stream<NursingStaff> searchNursingStaff(Predicate<NursingStaff> condition) {
		return hospital.getHospitalStaff().stream().filter(condition);
	}
	
	public static List<Patient> getHospitalizedPatients() {
		return hospital.getHospitalizedPatients();
	}
	
	public static List<Patient> getHospitalizablePatients() {
		return hospital.getHospitalizablePatients();
	}
}
