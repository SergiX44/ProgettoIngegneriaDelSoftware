package it.univr.terapiaintensiva.persistence;

import it.univr.terapiaintensiva.persistence.hospitalization.Hospitalization;
import it.univr.terapiaintensiva.persistence.hospitalization.Medicine;
import it.univr.terapiaintensiva.persistence.users.NursingStaff;
import it.univr.terapiaintensiva.persistence.users.Patient;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Hospital implements Serializable {
	
	private List<Patient> patients = new ArrayList<>();
	private List<NursingStaff> hospitalStaff = new ArrayList<>();
	private List<Medicine> medicines = new ArrayList<>();
	
	public Hospital() {
	}
	
	public List<Patient> getPatients() {
		return patients;
	}
	
	public Hospital setPatients(List<Patient> patients) {
		this.patients = patients;
		return this;
	}
	
	public List<NursingStaff> getHospitalStaff() {
		return hospitalStaff;
	}
	
	public Hospital setHospitalStaff(List<NursingStaff> hospitalStaff) {
		this.hospitalStaff = hospitalStaff;
		return this;
	}
	
	public List<Medicine> getMedicines() {
		return medicines;
	}
	
	public Hospital setMedicines(List<Medicine> medicines) {
		this.medicines = medicines;
		return this;
	}
	
	public List<Patient> getHospitalizedPatients() {
		return getPatients()
				.stream()
				.filter(p -> p.getHospitalizations().stream().anyMatch(h -> h.getDismissalLetter() == null))
				.collect(Collectors.toList());
	}
	
	public List<Patient> getHospitalizablePatients() {
		return getPatients()
				.stream()
				.filter(p -> p.getHospitalizations().isEmpty() || p.getHospitalizations().stream().noneMatch(h -> h.getDismissalLetter() == null))
				.collect(Collectors.toList());
	}
	
	
	public void hospitalize(Patient patient, String diagnosis) {
		Hospitalization hospitalization = new Hospitalization(diagnosis);
		try {
			patient.getHospitalizations().add(hospitalization);
			DataManager.getMonitor().addPatient(patient);
		} catch (InvocationTargetException e) {
			// rollback
			patient.getHospitalizations().remove(hospitalization);
		}
	}
	
	public void dismiss(Patient patient, String letter) {
		DataManager.getMonitor().stopMonitoring(patient);
		patient.getCurrentHospitalization().setDismissalLetter(letter);
	}
	
}
