package it.univr.terapiaintensiva.persistence.users;

import java.time.LocalDate;

public class Doctor extends NursingStaff {

    private String specialization;

    public Doctor(String firstName, String lastName, Sex sex, LocalDate birthDate, String birthPlace, String medId, String passwordHash, String specialization) {
        super(firstName, lastName, sex, birthDate, birthPlace, medId, passwordHash);
        this.specialization = specialization;
    }
    
    public String getSpecialization() {
        return specialization;
    }
    
    public Doctor setSpecialization(String specialization) {
        this.specialization = specialization;
        return this;
    }
	
	
	@Override
	public String toString() {
		return "Doctor{" +
				super.toString() +
				"specialization='" + specialization + '\'' +
				'}';
	}
}
