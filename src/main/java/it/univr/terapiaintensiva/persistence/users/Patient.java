package it.univr.terapiaintensiva.persistence.users;

import it.univr.terapiaintensiva.persistence.hospitalization.Hospitalization;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Patient extends User {
	
	private String healthCode;
	private List<Hospitalization> hospitalizations = new ArrayList<>();
	
	
	public Patient(String firstName, String lastName, Sex sex, LocalDate birthDate, String birthPlace, String healthCode) {
		super(firstName, lastName, sex, birthDate, birthPlace);
		this.healthCode = healthCode;
	}
	
	public List<Hospitalization> getHospitalizations() {
		return hospitalizations;
	}
	
	public Patient setHospitalizations(List<Hospitalization> hospitalizations) {
		this.hospitalizations = hospitalizations;
		return this;
	}
	
	public String getHealthCode() {
		return healthCode;
	}
	
	public Patient setHealthCode(String healthCode) {
		this.healthCode = healthCode;
		return this;
	}
	
	public Hospitalization getCurrentHospitalization() {
		return this.getHospitalizations().stream().filter(hospitalization -> hospitalization.getDismissalLetter() == null).findFirst().orElse(null);
	}
	
	
	@Override
	public String toString() {
		return this.getFirstName() + " " + this.getLastName() + " [" + this.healthCode + "]";
	}
}
