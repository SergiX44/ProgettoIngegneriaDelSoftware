package it.univr.terapiaintensiva.persistence.users;

import java.io.Serializable;
import java.time.LocalDate;

public abstract class User implements Serializable {
	
	private String firstName;
	private String lastName;
	private Sex sex;
	private LocalDate birthDate;
	private String birthPlace;

	public User(String firstName, String lastName, Sex sex, LocalDate birthDate, String birthPlace) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.sex = sex;
		this.birthDate = birthDate;
		this.birthPlace = birthPlace;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public Sex getSex() {
		return sex;
	}
	
	public void setSex(Sex sex) {
		this.sex = sex;
	}
	
	public LocalDate getBirthDate() {
		return birthDate;
	}
	
	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}
	
	public String getBirthPlace() {
		return birthPlace;
	}
	
	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}
	
	
	@Override
	public String toString() {
		return "User{" +
				"firstName='" + firstName + '\'' +
				", lastName='" + lastName + '\'' +
				", sex=" + sex +
				", birthDate=" + birthDate +
				", birthPlace='" + birthPlace + '\'' +
				'}';
	}
}
