package it.univr.terapiaintensiva.persistence.users;

import java.time.LocalDate;

public abstract class NursingStaff extends User {
	
    private String medId;
    private String passwordHash;

    public NursingStaff(String firstName, String lastName, Sex sex, LocalDate birthDate, String birthPlace, String medId, String passwordHash) {
        super(firstName, lastName, sex, birthDate, birthPlace);
        this.medId = medId;
        this.passwordHash = passwordHash;
    }

    public String getMedId() {
        return medId;
    }

    public NursingStaff setMedId(String medId) {
        this.medId = medId;
        return this;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public NursingStaff setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
        return this;
    }
    
    @Override
    public String toString() {
        return "NursingStaff{" +
                super.toString() +
                "medId='" + medId + '\'' +
                ", passwordHash='" + passwordHash + '\'' +
                "} ";
    }
}
