package it.univr.terapiaintensiva.persistence.users;

import java.time.LocalDate;

public class PrimaryDoctor extends Doctor {

    private String department;

    public PrimaryDoctor(String firstName, String lastName, Sex sex, LocalDate birthDate, String birthPlace, String medId, String passwordHash, String specialization, String department) {
        super(firstName, lastName, sex, birthDate, birthPlace, medId, passwordHash, specialization);
        this.department = department;
    }
    
    public String getDepartment() {
        return department;
    }
    
    public PrimaryDoctor setDepartment(String department) {
        this.department = department;
        return this;
    }
	
	
	@Override
    public String toString() {
        return "PrimaryDoctor{" +
                super.toString() +
                "department='" + department + '\'' +
                '}';
    }
}
