package it.univr.terapiaintensiva.persistence.users;

public enum Sex {
    
    MALE("Maschio"),
    FEMALE("Femmina");

    private String sex;

    Sex(String sex) {
        this.sex = sex;
    }
	
	
	@Override
    public String toString() {
        return sex;
    }
}
