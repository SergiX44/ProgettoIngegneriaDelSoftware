package it.univr.terapiaintensiva.persistence.users;

import java.time.LocalDate;

public class Nurse extends NursingStaff {
    
    public Nurse(String firstName, String lastName, Sex sex, LocalDate birthDate, String birthPlace, String medId, String passwordHash) {
        super(firstName, lastName, sex, birthDate, birthPlace, medId, passwordHash);
    }
    
    @Override
    public String toString() {
        return "Nurse{" +
                super.toString() +
                "}";
    }
}
