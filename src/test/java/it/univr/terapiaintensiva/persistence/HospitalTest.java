package it.univr.terapiaintensiva.persistence;

import it.univr.terapiaintensiva.monitoring.Vitals;
import it.univr.terapiaintensiva.persistence.users.Patient;
import it.univr.terapiaintensiva.persistence.users.Sex;
import org.junit.jupiter.api.TestInstance;

import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.stream.Collectors;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class HospitalTest {
	
	private ScheduledExecutorService printer = Executors.newSingleThreadScheduledExecutor();
	
	private Hospital hospital;
	private Patient mario = new Patient("Mario", "Rossi", Sex.MALE, LocalDate.now().minusYears(40), "Milano", "asdfgh");
	private Patient paola = new Patient("Paola", "Verdi", Sex.FEMALE, LocalDate.now().minusYears(30), "Roma", "dfgaas");
	private Patient marco = new Patient("Marco", "Violi", Sex.MALE, LocalDate.now().minusYears(20), "Genova", "hsfghs");

//	@BeforeAll
//	public void setUp() {
//		DataManager.setMonitor(new VitalsMonitor(DemoDataGenerator.class));
//		hospital = new Hospital();
//		hospital.getPatients().add(mario);
//		hospital.getPatients().add(paola);
//		hospital.getPatients().add(marco);
//	}
//
//	@Test
//	public void hospitalization() throws InterruptedException {
//		CountDownLatch cdl = new CountDownLatch(1);
//		hospital.hospitalize(mario, "Febbre alta, mal di stomaco");
//		printer.scheduleAtFixedRate(() -> printStatus(mario), 30, 30, TimeUnit.SECONDS);
//		hospital.hospitalize(paola, "Diarrea");
//		printer.scheduleAtFixedRate(() -> printStatus(paola), 30, 30, TimeUnit.SECONDS);
//		hospital.hospitalize(marco, "Dolori articolari");
//		printer.scheduleAtFixedRate(() -> printStatus(marco), 30, 30, TimeUnit.SECONDS);
//		printer.schedule(() -> {
//			printer.shutdownNow();
//			cdl.countDown();
//		}, 6, TimeUnit.MINUTES);
//		cdl.await();
//	}
//
//	@Test
//	public void shortHospitalization() throws InterruptedException {
//		CountDownLatch cdl = new CountDownLatch(1);
//		hospital.hospitalize(mario, "Febbre alta, mal di stomaco");
//		printer.scheduleAtFixedRate(() -> printStatus(mario), 30, 30, TimeUnit.SECONDS);
//		printer.schedule(() -> hospital.dismiss(mario, "Il paziente non presenta più i sintomi"), 3, TimeUnit.MINUTES);
//		printer.schedule(() -> {
//			printer.shutdownNow();
//			cdl.countDown();
//		}, 6, TimeUnit.MINUTES);
//		cdl.await();
//	}
	
	private static void printList(List<? extends Vitals.Reading> list) {
		System.out.println(String.join(", ", list.stream().map(Vitals.Reading::getValue).map(String::valueOf).collect(Collectors.toList())));
	}
	
	private static void printStatus(Patient patient) {
		if (patient.getCurrentHospitalization() == null) {
			System.out.println("Il paziente è stato dismesso");
			return;
		}
		printList(patient.getCurrentHospitalization().getVitals().getSBP());
		printList(patient.getCurrentHospitalization().getVitals().getDBP());
		printList(patient.getCurrentHospitalization().getVitals().getHeartRate());
		printList(patient.getCurrentHospitalization().getVitals().getTemperature());
		System.out.println(patient.getFirstName() + "'s " + patient.getCurrentHospitalization().getVitals().toString());
	}
	
}