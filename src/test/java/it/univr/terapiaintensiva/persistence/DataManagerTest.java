package it.univr.terapiaintensiva.persistence;

import it.univr.terapiaintensiva.demo.DemoDataGenerator;
import it.univr.terapiaintensiva.persistence.users.NursingStaff;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DataManagerTest {
	
	@Test
	void currentUser() {
		NursingStaff n = DemoDataGenerator.mockPrimaryDoctor();
		
		DataManager.setCurrentUser(n);
		
		assertEquals(n, DataManager.getCurrentUser());
	}
}
